# README #

Common ZCode framework

### What is this repository for? ###

* Initially developed for villagetoolbox.com - providing a rapid build solution for simple projects

Install composer.

Create a composer.json file:
```
{
    "require": {
        "zebraspot/zcode": "2.0.0-alpha.6"
    }
}
```
for added functionality, you may want to also add 
```
	"sendinblue/api-v3-sdk": "v8.0.0",
	"aws/aws-sdk-php": "^3.209"
```
Then, on the command line:
```
composer install
ln -s vendor/zebraspot/zcode/src/zc-assets html/zc-assets
```