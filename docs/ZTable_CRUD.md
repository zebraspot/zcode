# Step 1 - Create the table in the DB
Records using ZTable should have:
```
CREATE TABLE `myrecords` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `status` int NOT NULL DEFAULT '0',
  `user_id` int DEFAULT NULL,
  `created` bigint unsigned DEFAULT NULL,
  `modified` bigint unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
```
`status` can be any number. '0' is the default, and negative numbers typically mean a soft delete. Records with a negative status will not show up in ZTable class queries unless explicitly specified.

`user_id` is only used if the record class `$setCreator` set. 
```
public static $setCreator = true;
```

# Step 2 - Add the table to base.json
We typically have a utility tool at http://yoursite.com/utils that has a 'Save Schemas' option to display the results of `ZDB::BuildTableDescriptions(true);`.  This should be copied and pasted over the contents of /config/schema/base.json

# Step 3: Create your record class
Wherever you keep your php record classes, typically /html/includes/src, create a new file, and call it something like `MyRecord.class.php`:
```
<?php
	class MyRecord extends ZTable {
		public static $recordValues = [];
		public static $tableName = 'myrecords';
		public static $recordName = 'My Record';
		public static $setCreator = true;

	}

```
and add this file to the require block in your `/html/includes/site.php`
```
	require_once('src/MyRecord.class.php');
```

# Create your basic CRUD php files
Create a folder on your site to hold the basic CRUD files, at /html/myrecords/` for example.
```
/html
	/myrecords
		/index.php
		/edit.php
```
`index.php` is the list page, and will look something like:
```
<?php
	require_once('site.php');
	
	// Requires users to be logged in to see this page.
	ZAuth::CheckUser(); 
	
	ZRequest::Header('My Records');

	?>
		<a href="edit.php" class="btn">Create New My Record</a>
	<?php
	echo MyRecord::GetTable([], ['tableClass' => 'myrecords']);

	ZRequest::Footer();
```
`edit.php` is the record form page for create and edit.
```
<?php
	require_once('site.php');
	ZAuth::CheckUser(); 

	if ($myrecord = MyRecord::saveForm()) {
		ZRequest::Redirect('/myrecords/');
	}

	ZRequest::Header('Edit My Record');
	$myrecord = MyRecord::GetOrNew(@$_GET['myrecord']);
	$myrecord->renderForm();

	ZRequest::Footer();
```
This will render an input form that can be used for creating new records and editing existing ones. To customize the form, you can add a number of options to the file /config/schema/custom.json
```
{
	"myrecords": {

		// Layout for form...
		"fieldToHide": { "renderoOnForm": false },
		"firstField": {  "sort": 1 },
		"secondField": {  "sort": 2 },
		"layingOutFields": { "layout": "section:Details, tab:Info, row:main" },
		"addingAClassToField": { "class": "important-field" },

		// Basic Field Attributes...
		"requiredField": { "required": true },
		"fieldWithADefault" { "default": "20" },
		"readOnlyField": { "readonly": true },
		"disabledField": { "disabled": true },

		// Number fields...
		"slider": { "input": "slider", "min": 0, "max" : 5 },
		"currencyFloat": { "min": 0, "step" : "0.01" },

		// Dynamic code-driven fields...
		"selectField": { "input": "select", "selectOptions": { "0": "No", "1": "Yes"}},
			// Dynamically override available options with $myrecord->GetSelectOptions($colName);
		"injectAnyHtml" : { "type": "virtual", "input": "html", "html":"<span>Boo!</span>" },
			// Dynamically override html contents with $myrecord->GetFormHtml($colName);
		"foreignKeyLink": { "type": "ref" },
			// Dynamically override reference link with $myrecord->GetRefLink($colName);

		// Other field types...
		"formattedString": { "stringMask": "/^[a-z]{2}\\-?[A-Z]{0,5}$/", "maxLength": 8 },
		"richTextArareaUsingTinyMCE": { "rich": true },
		"dateField": { "input": "date" },
		"fieldThatGetsCursorOnPageLoad": { "autofocus": true },
		"autocompleteField": { "autocomplete": true },
		"textAreaThatExpectsJSON": { "json": true }
	}


}
```
