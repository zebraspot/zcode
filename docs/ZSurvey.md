# zcode/ZTable Class
The Survey class is a basic survey tool. 

## Specifying the survey contents
Create a json file...: 
```
{
	"q1-intro":{"type": "info"},
	"q2-how-like-wings":{"type": "radio", "options": ["baked", "fried", "BBQed"]},
	"q3-how-feel":{"type": "select", "options": ["happy", "sad", "angry"]},
	"q3-how-experience":{"type": "rate5", "instructions": "rate5-bad-to-excellent"},
	"q4-would-come-back":{"type": "yesno"},
	"q5-email":{"type": "text", "text-type": "email"},
	"q5-when-last-perm":{"type": "text", "text-type": "date", "freetext": false},
	"q6-why-so-serious":{"type": "textarea"},
	"q5-buhbye":{"type": "done"}
}
```
## Create a table for responses...
```
CREATE TABLE `survey_responses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(16) DEFAULT NULL,
  `answers` text,
  `dateStarted` bigint(20) NOT NULL DEFAULT '0',
  `dateCompleted` bigint(20) NOT NULL DEFAULT '0',
  `surveyId` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```

## Load the survey...
```
<?php
	require_once('site.php');
	ZCode::require('ZSurvey');
	
	$survey = new ZSurvey('test', ['type' => 'json', 'source' =>'data/test.json']);

	if (!empty($_POST['survey'])) {
		if ($survey->saveFullResponse($_POST['survey'])) {
			ZRequest::Redirect('thanks.php');
		}
	}

	ZRequest::Header('Survey Test');
	echo $survey->getFullSurveyForm();
	ZRequest::Footer();
```

## View the results...
```
<?php
	require_once('site.php');
	require_once('vendor/zebraspot/zcode/src/ZSurvey.class.php');
	$survey = new ZSurvey('test', ['type' => 'json', 'source' =>'data/test.json']);

	ZRequest::Header('Survey Test');
	echo $survey->getSurveySummary();
	ZRequest::Footer();
```
