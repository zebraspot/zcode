# ZRequest
## TineMCE configuration
The bulk of tinyMCE setup is done in zc-assets/zcode.js
You can add parameters by setting the value of zcode.tinyParams
```
	ZCode::addToZCodeForJS('tinyParams', [
		'content_css' => '/css/tinymce.css',
		'toolbar' => 'styleselect | bold italic underline strikethrough | bullist numlist'
	]);
```