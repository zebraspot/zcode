<?php
	require_once(__DIR__.'/src/ZConstants.php');
	require_once(__DIR__.'/src/ZBase.class.php');
	require_once(__DIR__.'/src/ZUtils.class.php');
	require_once(__DIR__.'/src/ZDB.class.php');
	require_once(__DIR__.'/src/ZAwsS3.class.php');
	require_once(__DIR__.'/src/ZAwsSes.class.php');
	require_once(__DIR__.'/src/ZLog.class.php');
	require_once(__DIR__.'/src/ZRequest.class.php');
	require_once(__DIR__.'/src/ZForm.class.php');
	require_once(__DIR__.'/src/ZGD.class.php');
	require_once(__DIR__.'/src/ZAuth.class.php');
	require_once(__DIR__.'/src/ZMessage.class.php');
	require_once(__DIR__.'/src/ZI18n.class.php');
	require_once(__DIR__.'/src/ZValidate.class.php');
	require_once(__DIR__.'/src/ZTable.class.php');
	require_once(__DIR__.'/src/ZUser.class.php');
	require_once(__DIR__.'/src/ZUserGroup.class.php');
	require_once(__DIR__.'/src/ZWidgets.class.php');
	
	class ZCode extends ZBase {
		public static $version = '2.0.0-alpha.19';
		public static $beta = false;
		public static $debug = false;
		public static $sessionName = 'site';
		public static $path = '/var/www/site.com/html/';
		public static $domain = 'site.com';
		public static $mailDomain = null;
		public static $siteName = 'site.com';
		public static $root = 'http://site.com/';
		public static $zcodePath = '/includes/zcode/';
		public static $mailFromName = null;
		public static $zCodeForJS = [];

		public static function Init() {
			date_default_timezone_set('America/Toronto');
			
			global $skipSession;
			
			self::Config('site');
			
			if (!$skipSession) {
				session_start();
			}

			if (self::$beta) {
				error_reporting(E_ALL ^ E_NOTICE);
			} else {
				error_reporting(E_STRICT);
			}

			ZDB::Init();
			ZAuth::Init();
			ZRequest::Init();
			ZI18n::Init();
			ZMessage::Init();
			ZForm::Init();
			ZLog::Init();
			ZUtils::Init();
			ZAwsS3::Init();
			ZAwsSes::Init();
		}

		public static function require($class) {
			require_once(__DIR__ . '/src/' . $class . '.class.php');
		}

		public static function checkVersion($v) {
			if (static::$version != $v) {
				throw new Exception("ZCode Version $v is Required. Currently available: ".static::$version, 1);
				
			}
		}

		public static function GetZCodeForJS() {	
			$data = array(
				'path' => ZCode::$zcodePath,
				'lang' => ZI18n::GetLang(),
				'i18n' => ZI18n::GetJSTranslations()
			);
			return array_merge($data, static::$zCodeForJS);
		}

		public static function addToZCodeForJS($key, $value) {
			static::$zCodeForJS[$key] = $value;
		}
		
	}
	
	ZCode::Init();