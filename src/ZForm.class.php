<?php

	class ZForm extends ZBase {
		public static $reCaptchaSiteKey;
		public static $reCaptchaSecretKey;
        public static $reCaptchaFailureRedirectURL;

		private $tableName;
		public $columnData;
		private $options;
		private $action;
		private $method;
		private $displayNameField = 'name';
		private $object;

		public static function Init() {
			self::Config('form');
		}

		function __construct($object, $options = [], $columnData = null) {
			if (is_string($object)) {
				$this->tableName = $object;
			} else {
				$this->object = $object;
				$this->tableName = $object::$tableName;
			}
			if ($columnData) {
				$this->columnData = $columnData;
			} else {
				$columnDefaults = [
					'id' => ['renderOnForm' => false],
					'created' => ['renderOnForm' => false],
					'modified' => ['renderOnForm' => false],
					'user_id' => ['input' => 'ref'],
				];
				$this->columnData = array_replace_recursive($columnDefaults, ZDB::GetTableDescriptionData($this->tableName));
			}
			$this->options = $options;
			$this->action = '';
			$this->method = 'post';
		}



		//     .d8888. d888888b  .d8b.  d888888b d888888b  .o88b.      d88888b  .d88b.  d8888b. .88b  d88. 
		//     88'  YP `~~88~~' d8' `8b `~~88~~'   `88'   d8P  Y8      88'     .8P  Y8. 88  `8D 88'YbdP`88 
		//     `8bo.      88    88ooo88    88       88    8P           88ooo   88    88 88oobY' 88  88  88 
		//       `Y8b.    88    88~~~88    88       88    8b           88~~~   88    88 88`8b   88  88  88 
		//     db   8D    88    88   88    88      .88.   Y8b  d8      88      `8b  d8' 88 `88. 88  88  88 
		//     `8888Y'    YP    YP   YP    YP    Y888888P  `Y88P'      YP       `Y88P'  88   YD YP  YP  YP 
																									
																											
		public static function GetSelectTag($name, $options, $value, $params = array()) {
			$attributes = self::GetAttributesString($params);
			$out = '<select name="'.$name.'" id="'.self::asId($name).'" '.$attributes.' >';
			foreach ($options as $key => $optName) {
				$out .= '<option value="'.trim($key).'"';
				if ($key === $value) $out .= ' SELECTED="SELECTED"';
				$out .= '>'.$optName.'</option>';
			}
			$out .= '</select>';
			return $out;
		}

	
		public static function GetRichSelectTag($name, $options, $value, $params = array()) {
			$class = isset($params['attr']['class']) ? $params['attr']['class'] : '';
			unset($params['attr']['class']);
			$out = '<select name="'.$name.'" id="'.self::asId($name).'" class="rich-menu '.$class.'" ' . self::GetAttributesString($params) . ' >';
			$out .= self::GetRichSelectOptions($options, $value);
			$out .= '</select>';
			return $out;
		}
	
		public static function GetRichSelectOptions($options, $value) {
			if (empty($options)) {
				return '';	
			}
			$out = '';
			foreach ($options as $key => $option) {
				if ($option['type'] == 'opt') {
					$out .= self::GetRichSelectOption($key, $option, $value);
					$out .= self::GetRichSelectOptions($option['items'], $value);
				} else {
					$out .= '<optgroup label="'.$option['name'].'">';
					$out .= self::GetRichSelectOptions($option['items'], $value);
					$out .= '</optgroup>';
				}
			}
			return $out;
		}
		
		public static function GetRichSelectOption($key, $option, $value) {
			$out = '<option value="'.$key.'" ';
			if ($key == $value) {
				$out .= ' SELECTED="SELECTED"';
			}
			$attributes = self::GetAttributesString($option);
			$out .= $attributes . ' >'.$option['name'].'</option>';
			return $out;
		}

		public static function GetTextTag($type, $name, $value, $params=array()) {
			$attributes = self::GetAttributesString($params);
			return '<input type="'.$type.'" name="'.$name.'" id="'.self::asId($name).'" value="' . htmlspecialchars($value) . '" '.$attributes.' />';
		}
	
		public static function GetTextArea($name, $value, $params=array()) {
			$attributes = self::GetAttributesString($params);
			return '<textarea name="'.$name.'" id="'.self::asId($name).'" '.$attributes.' >' . htmlspecialchars($value) . '</textarea>';
		}

		public static function asId($str) {
			return preg_replace('/[^a-zA-Z\d]/', '-', $str);
			// return preg_replace('/[^abcdefghijklmnopqrstuvwxyz123456789]/','',strtolower($val));
		}
	
		public static function GetRichTextArea($name, $value, $params=array()) {
			if (empty($params['attr'])) {
				$params['attr'] = array();
			}
			if (empty($params['attr']['class'])) {
				$params['attr']['class'] = 'rich';
			} else {
				$params['attr']['class'] .= ' rich';
			}
			return self::GetTextArea($name, $value, $params);
		}
	
		public static function GetSubmitButton($value, $params = array()) {
			$out = '<input type="submit" name="submit" value="'.$value.'" ' . self::GetAttributesString($params) . ' />';
			return $out;
		}

		public static function GetWhenceTag($fallback = null) {
			$whence = FirstNotEmpty([@$_POST['whence'], @$_GET['whence'], @$_SERVER['HTTP_REFERER'], $fallback, '/']);
			$whence = str_replace(trim(ZCode::$root,'/'), '', $whence);
			return static::GetTextTag('hidden', 'whence', $whence, ['attr' => ['id' => 'whence']]);
		}

		public static function GetCheckboxTag($name, $value, $params = array()) {
			$out = '<input type="checkbox" name="'.$name.'" value="1" ' . self::GetAttributesString($params) . ' ';
			if (!empty($value)) {
				$out .= 'checked="checked"';
			}
			$out .= " />";
			return $out;
		}

		// Create Sites and register API keys at https://www.google.com/recaptcha/admin/
		public static function GetCaptcha() {
			self::CheckReCaptcha();
			return '<script src="https://www.google.com/recaptcha/api.js" async defer></script>
			<div class="g-recaptcha" data-sitekey="'.self::$reCaptchaSiteKey.'"></div>';
		}

		public static function ValidateCaptcha($errorOnFail = true) {
			$reCaptchaPost = array(
                'secret' => self::$reCaptchaSecretKey,
                'response' => $_POST['g-recaptcha-response'],
                'remoteip' => $_SERVER['REMOTE_ADDR']
	        );

			$reCaptchaURL = 'https://www.google.com/recaptcha/api/siteverify';
    	    $reCaptchaResponse = ZUtils::PostCurl($reCaptchaURL, $reCaptchaPost, array('dataType' => 'json'));

			if ($reCaptchaResponse['success'] !== 'true' && $reCaptchaResponse['success'] !== true) {
				if ($errorOnFail) {
					if (!empty(self::$reCaptchaFailureRedirectURL)) {
						ZRequest::Redirect(self::$reCaptchaFailureRedirectURL);
					}
					ZRequest::Error('Recaptcha Verification Failed');
				}
				return false;
			}
			return true;
		}

		private static function CheckReCaptcha() {
			if (empty(self::$reCaptchaSiteKey) || empty(self::$reCaptchaSecretKey)) {
				ZRequest::Error('ReCaptcha not configured in ZConfig');
			}
		}
		public static function GetAttributesString($params) {
			$attributes = '';
			$quotes = empty($params['quotes']) ? '"' : $params['quotes'];
			if (!empty($params['attr'])) {
				foreach ($params['attr'] as $attrName => $attrValue) {
					$attributes .= $attrName . '='.$quotes.$attrValue.$quotes.' ';
				}
			}
			return $attributes;
		}


		//    d8888b. d88888b d8b   db d8888b. d88888b d8888b.      d88888b  .d88b.  d8888b. .88b  d88. 
		//    88  `8D 88'     888o  88 88  `8D 88'     88  `8D      88'     .8P  Y8. 88  `8D 88'YbdP`88 
		//    88oobY' 88ooooo 88V8o 88 88   88 88ooooo 88oobY'      88ooo   88    88 88oobY' 88  88  88 
		//    88`8b   88~~~~~ 88 V8o88 88   88 88~~~~~ 88`8b        88~~~   88    88 88`8b   88  88  88 
		//    88 `88. 88.     88  V888 88  .8D 88.     88 `88.      88      `8b  d8' 88 `88. 88  88  88 
		//    88   YD Y88888P VP   V8P Y8888D' Y88888P 88   YD      YP       `Y88P'  88   YD YP  YP  YP 
																								  



		function render($data = null) {
			if (!$data && $this->object) {
				$data = $this->object->data;
			}
			$html = '<form class="zform zform-'.$this->tableName.'" action="' . $this->action . '" method="' . $this->method . '"';
			if (isset($this->options['enctype'])) {
				$html .= ' enctype="' . $this->options['enctype'] . '"';
			}
			$html .= '>';
			if (!empty($data['id'])) {
				$html .= '<h2>';
				$html .= i18n('formtitle-edit-'.$this->tableName, ['recordTitle' => $this->object->getDisplayName()]);
				// $html .= ': '.$this->data[$this->displayNameField]
				$html .= '</h2>';
				$html .= self::GetTextTag('hidden', $this->tableName."[id]", $data['id']);
			} else {
				$html .= '<h2>'.i18n('formtitle-new-'.$this->tableName).'</h2>';
			}
			$html .= $this->getSubmitRow('upper');
			$html .= $this->getFieldsHtml($data);
			$html .= $this->getSubmitRow('lower');
			$html .= static::GetWhenceTag();
			$html .= '</form>';
			echo $html;
		}

		function getFieldsHtml($data) {
			$this->data = $data;
			$out = '';
			// pr($this->columnData);
			$this->sortColumnData();
			$out .= $this->getFormSectionHtml($this->layoutTree);
			return $out;
		}

		function getFormSectionHtml($sections) {
			$out = '';
			foreach ($sections as $sectionName => $tabs) {
				if ($sectionName != 'none') {
					$out .= '<div class="zform-section" id="zform-section-'.$this->tableName.'-'.$sectionName.'">';
					$out .= '<h2>'.i18n('zform-section-title-'.$this->tableName.'-'.$sectionName).'</h2>';
				}
				$out .= $this->getFormTabHtml($tabs, $sectionName);
				if ($sectionName != 'none') {
					$out .= '</div>';
				}
			}
			return $out;
		}

		function getFormTabHtml($tabs, $sectionName) {
			$tabTags = '';
			$tabContents = '';
			$noTabContents = '';
			$tabClass = 'current';
			foreach ($tabs as $tabName => $rows) {
				if ($tabName != 'none') {
					$tabId = $this->tableName.'-'.$sectionName.'-'.$tabName;
					$tabTags .= '<span class="zform-tab-tag '.$tabClass.'" data-tab="zform-tab-'.$tabId.'">'.i18n('zform-tab-title-'.$tabId).'</span>';
					$tabContents .= '<div class="zform-tab '.$tabClass.'" id="zform-tab-'.$tabId.'">';
					$tabContents .= $this->getFormRowHtml($rows);
					$tabContents .= '</div>';
					$tabClass = '';
				} else {
					$noTabContents .= $this->getFormRowHtml($rows);
				}
			}
			return $noTabContents.'<div class=""zform-tabs"><div class="zform-tab-tags">'.$tabTags.'</div>'.$tabContents.'</div>';
		}

		function getFormRowHtml($rows) {
			$out = '';
			foreach ($rows as $rowName => $fields) {
				$out .= '<div class="zform-row" id="zform-row-'.$this->tableName.'-'.$rowName.'">';
				foreach ($fields as $colName) {
					$out .= $this->getFieldInputHtml($colName, $this->columnData[$colName]);
				}
				$out .= '</div>';
			}
			return $out;
		}

		function getSubmitRow($class='') {
			$out = '<div class="form-element submit '.$class.'">';
			$out .= '<input class="btn btn--primary" type="submit" name="save" value="save"> ';
			if ($this->object->fromDB && $this->object->canDelete()) {
				$out .= '<input class="btn" type="submit" name="delete" value="delete"  onclick="return confirm(\''.i18nRaw('ZC-form-confirm-delete', ['recordName' => $this->object->getRecordName()]).'\');"> ';
			}
			$out .= '<input class="btn" type="submit" name="cancel" value="cancel" onclick="return ZUtils.goWhence();">';
			$out .= '</div>';
			return $out;
		}

		function getFieldInputHtml($colName, $colData) {
			$params = $this->tableDataToFieldAttributes($colName, $colData);
			$label = isset($colData['label']) ? $colData['label'] : i18n('formfield_'.$this->tableName.'_'.$colName);
			$pre = $this->getFormAspect($colName, 'prepend');
			$input = $pre . $this->getFieldInput($colName, $colData, $params);
			if (empty($input)) return '';
			$required = isset($params['attr']['required']) ? 'required' : '';
			$subtext = $this->getFormAspect($colName, 'subtext');
			if (!empty($subtext)) {
				$subtext = '<div class="subtext">'.$subtext.'</div>';
			}
			$out = '<div class="form-element form-element-'.$colName.' '.$required.'"><label>'.$label.'</label>' .$input.$subtext.'</div>';
			return $out;
		}
		
		function getFormAspect($colName, $aspectName, $asRaw = false) {
			if (isset($colData[$aspectName])) {
				return $colData[$aspectName];
			}
			$stringkey = $this->tableName.'_'.$colName.'_'.$aspectName;
			if ($asRaw) {
				return i18nRaw($stringkey, [], ['optional' => true]);
			}
			return i18n($stringkey, [], ['optional' => true]);
		}

		function getFieldInput($colName, $colData, $params) {
			if (isset($colData['html'])) {
				return $colData['html'];
			}
			if (@$colData['input'] == 'html') {
				return $this->object->GetFormHtml($colName);
			}
			if (!isset($this->data[$colName]) && isset($colData['default'])) {
				$this->data[$colName] = $colData['default'];
			}
			if (@$colData['input'] == 'ref') {
				return $this->object->GetRefLink($colName) . self::GetTextTag('hidden', $this->tableName."[$colName]", @$this->data[$colName]);
			} else if (@$colData['input'] == 'select') {
				$options = $this->GetSelectOptions($colName, $colData);
				// pr(@$this->data[$colName], $colName);
				// var_dump($options);
				// var_dump($this->data);
				return self::GetSelectTag($this->tableName."[$colName]", $options, @$this->data[$colName], $params );
			} else if (@$colData['type'] == 'varchar' || @$colData['type'] == 'text') {
				if (@$colData['input'] == 'textarea' || $colData['type'] == 'text') {
					if (@$colData['rich']) {
						return self::GetRichTextArea($this->tableName."[$colName]", @$this->data[$colName], $params);
					} else {
						return self::GetTextArea($this->tableName."[$colName]", @$this->data[$colName], $params);
					}
				} else {
					return self::GetTextTag('text', $this->tableName . "[$colName]", @$this->data[$colName], $params);
				}
			} else if (in_array($colData['type'], ['tinyint', 'int', 'bigint', 'float', 'decimal'])) {
				if (@$colData['input'] == 'xxxxx') {
					return 'datpicker';
				} else {
					$value = $this->data[$colName];
					$type = 'number';
					
					if (@$colData['input'] == 'slider') { $type = 'range'; }
					else if (@$colData['input'] == 'date') { 
						$type = 'date'; 
						if (!empty($value)) { $value = date('Y-m-d', $value); }
					}
					
					$params = $this->addNumberAttributes($params, $colData);
					return self::GetTextTag($type, $this->tableName."[$colName]", $this->data[$colName], $params);
				}
			} else {
				pr($colData, 'Render method not found for column ' . $colName);
			}
		}
		
		function GetSelectOptions($colName, $colData) {
			if (isset($colData['selectOptions'])) {
				return $colData['selectOptions'];
			}
			if ($this->object) {
				$options = $this->object->GetSelectOptions($colName);
				if ($options) {
					return $options;
				}
			}
		}

		function tableDataToFieldAttributes($colName, $colData) {
			$attr = [];
			if (isset($colData['maxlength'])) { $attr['maxlength'] = $colData['maxlength']; }
			else if ($colData['type'] == 'varchar') { $attr['maxlength'] = $colData['len']; }

			if (@$colData['required'] || (@$colData['allowNull'] == '0' && empty($colData['default']))) { $attr['required'] = ''; }
			if (@$colData['readonly']) { $attr['readonly'] = ''; }
			if (@$colData['disabled']) { $attr['disabled'] = ''; }
			if (@$colData['autofocus']) { $attr['autofocus'] = ''; }
			if (@$colData['autocomplete']) { $attr['autocomplete'] = $colData['autocomplete']; }
			if (@$colData['pattern']) { $attr['pattern'] = $colData['pattern']; }
			if (@$colData['class']) { $attr['class'] = strval($colData['class']); }
			if (@$colData['json']) { $attr['class'] .= ' json-input'; }
			if (@$colData['type'] == 'varchar' || @$colData['type'] == 'text') {
				$attr['placeholder'] = $this->getFormAspect($colName, 'placeholder', true);
				if (isset($colData['stringMask'])) { 
					$attr['data-string-mask'] = trim($colData['stringMask'], '/');
					$attr['data-string-mask-failed'] = $this->getFormAspect($colName, 'stringMaskFailed', true);
				}
			}
						
			$params = ['attr' => $attr];
			return $params;
		}

		function addNumberAttributes($params, $colData) {
			if (isset($colData['min'])) { 
				$params['attr']['min'] = $colData['min'];
			} else if (@$colData['unsigned']) {
				$params['attr']['min'] = 0;
			}
			if (isset($colData['step'])) {
				$params['attr']['step'] = $colData['step'];
			} else if (@$colData['type'] == 'float' || @$colData['type'] == 'decimal') {
				$params['attr']['step'] = '0.01';
			}
			if (isset($colData['max'])) {
				$params['attr']['max'] = $colData['max'];
			}
			return $params;
		}

		function sortColumnData() {
			$colRefs = [];
			$this->rowId = 0;
			foreach ($this->columnData as $colName => $colData) {
				if (@$colData['renderOnForm'] === false || empty($colData['type'])) { continue; };
				$colRefs[] = [
					'name' => $colName,
					'sort' => isset($colData['sort']) ? $colData['sort'] : 1000000,
					'layout' => $this->parseLayout(@$colData['layout'])
				];
			}
			usort($colRefs, function ($a, $b) { return ($a['sort'] <=> $b['sort']); });
			foreach ($colRefs as $colRef) {
				$this->addToLayoutTree($colRef);
			}
		}

		function addToLayoutTree($colRef) {
			$section = $colRef['layout']['section'];
			$tab = $colRef['layout']['tab'];
			$row = $colRef['layout']['row'];
			if (!isset($this->layoutTree[$section])) $this->layoutTree[$section] = [];
			if (!isset($this->layoutTree[$section][$tab])) $this->layoutTree[$section][$tab] = [];
			if (!isset($this->layoutTree[$section][$tab][$row])) $this->layoutTree[$section][$tab][$row] = [];
			$this->layoutTree[$section][$tab][$row][] = $colRef['name'];
		}

		function parseLayout($layoutString) {
			$layout = ['section' => 'none', 'tab' => 'none', 'row' => 'r'.$this->rowId++];
			if (!empty($layoutString)) {
				foreach (explode(',', $layoutString) as $layer) {
					list($level, $name) = explode(':', $layer);
					$level = trim($level);
					if (empty($layout[$level])) { ZRequest::Error('Unknown Form layout level: ' . $level); }
					$layout[$level] = trim($name);
				}
			}
			return $layout;
		}

		//       d8888b. d88888b d8888b. .d8888. d888888b .d8888. d888888b    
		//       88  `8D 88'     88  `8D 88'  YP   `88'   88'  YP `~~88~~'  
		//       88oodD' 88ooooo 88oobY' `8bo.      88    `8bo.      88     
		//       88~~~   88~~~~~ 88`8b     `Y8b.    88      `Y8b.    88     
		//       88      88.     88 `88. db   8D   .88.   db   8D    88    
		//       88      Y88888P 88   YD `8888Y' Y888888P `8888Y'    YP    
                                                                                                   
                                                                                                   
		function getPostData() {
			if (empty($_POST[$this->tableName])) {
				ZRequest::Error(i18n('record-data-not-found'));	
			}
			$data = $_POST[$this->tableName]; 
			foreach ($data as $colName => $value) {
				$value = $this->processPostValue($colName, $value);
			}
			return $_POST[$this->tableName];
		}

		private function processPostValue($colName, $value) {
			$input = strval(@$this->columnData[$colName]['input']);
			if (isset($this->columnData[$colName]['transform'])) {
				if (!static::$customTransformer) { ZRequest::Error('ZForm::$customTransformer not set, but required for '.$this->tableName.' -> '.$colName);}
				return static::$customTransformer->process($colName, $this->columnData[$colName], $value);
			}
			if ($input == 'date') {
				return strtotime($value);
			}
			return $value;
		}

		public static function PrintHiddenInputs($params, $parent = '') {
			foreach ($params as $key => $val) {
				if (!empty($parent)) {
					$key = $parent . '[' . $key . ']';
				}
				if (is_array($val)) {
					static::PrintHiddenInputs($val, $key);
				} else {
					echo '<input type="hidden" name="' . $key . '" value="' . htmlspecialchars($val) . '" />';
				}
			}
		}

	}
