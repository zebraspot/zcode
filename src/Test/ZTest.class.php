<?php
	require_once(__DIR__.'/../../ZCode.php');
	require_once(__DIR__.'/ZAssert.class.php');
	require_once(__DIR__.'/ZTestUtils.class.php');
	require_once(__DIR__.'/ZTestConstants.php');
	
	class ZTest extends ZBase {
		public static $Assert;
		public static $Utils;
		public static $testFileSubstring = 'test_';

		public static function Init() {
			self::Config('test');
			if (!ZCode::$beta) {
				ZRequest::Error('test suite will alter the database.  Please run on Beta only');
			}
		}
		
		public static function TestPage() {
			require(__DIR__.'/inc/header.php');
				if (empty($_POST['tests'])) {
					self::TestSelectionForm();
				} else {
					self::RunTests();
				}
			require(__DIR__.'/inc/footer.php');
		}
		
		public static function getTests() {
			$tests = scandir('tests/');
			$tests = array_filter($tests, function($k) {
				if (strlen($k) < 3) {
					return false;
				}
				if (strpos($k, static::$testFileSubstring) === false) {
					return false;
				}
				return true;
			});
			return $tests;
		}

		public static function TestSelectionForm() {
			$tests = static::getTests();
			?>
				<div>Select the tests you want to run...</div>
				<form action="" method="post">
					<?php
					foreach ($tests as $test) {
						?>
							<input 
								type="checkbox" 
								checked="checked" 
								name="tests[]" 
								value="<?=$test; ?>" 
							/>
							<?=$test; ?>
							<?= stripos($test, 'log_in') !== false ? ' (required for log-in)' : ''; ?>
							<br />
						<?php
					 }
					?>
					<br />
					Override Admin Email: 
					<input type="text" name="adminEmail" value="<?=ZAuth::$adminEmail; ?>" /><br />
					<!-- 				
						<input 
							type="checkbox" 
							checked="checked" 
							name="cleanup" 
							value="1" 
						/> 
						Clean Up after tests<br /> 
					-->
					<br />
					<input type="submit" name="RunTests" value="Run Tests" />
				</form>
			<?php	
		}
		
		public static function RunTests() {
			global $responseData;
			$tests = static::getTests();
			$testsPassed = 0;
			$testsFailed = 0;

			foreach($_POST['tests'] as $test) {
				if (array_search($test, $tests)) {
		 			pr('Loading Test File', 'Test File: ./tests/'.$test);			
					require_once('tests/'.$test);
				}
			}
			
			?>
			<div class="summary-box"><?php
				ZTestUtils::Message('Tests Passed', ZAssert::$testsPassed);
				ZTestUtils::Message('Tests Failed', ZAssert::$testsFailed, ZAssert::$testsFailed > 0 ? 'bad':'good');
			?></div>
			<script>
				$(document).ready(function(){
					$('.response-log button.show').click(function(){
						$(this).parent().find('.response').show();
						$(this).hide();
					})
				});
			</script>				
			<?php
			
		}
		
		
	}
	
	ZTest::Init();