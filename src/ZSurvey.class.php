<?php
	class ZSurvey {
		public $questions;
		private $surveyId;

		private $surveyStats;
		private $filter;
		private $totalSurveys;
		private $filteredSurveys;

		public function __construct($surveyId, $options) {
			$this->surveyId = $surveyId;
			if ($options['type'] && $options['source']) {
				$this->loadQuestions($options['type'], $options['source']);
			} else if ($options['questions']) {
				$this->questions = $options['questions'];
			} else {
				throw New Exception('no questions found');
			}
			ZRequest::AddScript('/zc-assets/js/zsurvey.js');
		}

		private function loadQuestions($type, $source) {
			if ($type == 'json') {
				$this->questions = json_decode(file_get_contents($source), true);
				if (json_last_error()) {
					ZRequest::Error('JSON Load Error ('.$source.'): '. json_last_error_msg());
				}
			}
		}

		public function getFullSurveyForm() {
			$out = '<form method="post" id="zsurvey-form" class="zsurvey-form zsurvey-multiple" onsubmitx="return zSurveyCheckForm();" >';
			$out .= '<script>const ZSurveyData = '.json_encode($this->questions).';</script>';
			foreach ($this->questions as $questionId => $question) {
				$out .= $this->getSingleQuestion($questionId);
			}
			$out .= '<div class="submit-area"><button type="submit" form="zsurvey-form" class="button">' . i18n('zsurvey-submit-button') . '</button></div>';
			// $out .= '<input type="hidden" name="checksum" value="'.$this->getChecksum().'">';
			$out .= '</form>';
			return $out;
		}
		

		public function getSingleQuestionForm($questionId, $responseRecord) {
			$out = '<form method="post" id="zsurvey-form zsurvey-single" onsubmit="return zSurveyCheckForm();" >';
			$out .= $this->getSingleQuestion($questionId);
			$out .= '<div class="submit-area"><button type="submit" form="kin-survey-form" class="button">' . i18n('start-button') . '</button></div>';
			$out .= '</form>';
			return $out;
		}

		private function getSingleQuestion($questionId) {
			$question = $this->questions[$questionId];
			if ($question['type'] == 'html') {
				return $question['contents'];
			}
			if ($this->questions[$questionId]['default']) {
				$response = $this->questions[$questionId]['default'];
			}
			$out = '<div class="question-block question-block-'.$question['type'].'" id="question-block-'.$questionId.'"><div class="question-text">'.$this->getQuestionText($questionId).'</div>';
			if (!empty($question['instructions'])) {
				$out .= '<div class="question-instruct">'.i18n($question['instructions']).'</div>';
			}
			$out .= '<div class="input-area">';
			if ($question['type'] == 'info') {
				// $out .= $this->getInfoInput($questionId, $response);
			} elseif ($question['type'] == 'select') {
				$out .= $this->getSelectInput($questionId, $response);
			} elseif ($question['type'] == 'radio') {
				$out .= $this->getRadioInput($questionId, $response);
			} elseif ($question['type'] == 'rate5') {
				$out .= $this->getRate5Input($questionId, $response);
			} elseif ($question['type'] == 'yesno') {
				$out .= $this->getYesNoInput($questionId, $response);
			} elseif ($question['type'] == 'text') {
				$out .= $this->getTextInput($questionId, $response);
			} elseif ($question['type'] == 'textarea') {
				$out .= $this->getTextAreaInput($questionId, $response);
			} elseif ($question['type'] == 'done') {
				// $out .= $this->getDoneInput($questionId, $response);
			} else {
				pr($question, $questionId);
				ZRequest::Error('Unknown Question Type: ', $question);
			}
			$out .= '</div></div>';
			return $out;
		}

		private function isQuestionFreetext($questionId) {
			$question = $this->questions[$questionId];
			if (isset($question['freetext'])) {
				return $question['freetext'];
			}
			return $question['type'] == 'text' || $question['type'] == 'textarea';
		}

		private function acceptsResponse($questionId) {
			$questionType = $this->questions[$questionId]['type'];
			return $questionType != 'info' && $questionType != 'done' && $questionType != 'html';
		}

		private function getAnswerName($questionId) {
			return 'survey[answers]['.$questionId.']';
		}
		// private function getInfoInput($questionId, $response) {
		// 	$out = '<input type="hidden" name="answer" value ="1" />';
		// 	return $out;
		// }
		

		private function getSelectInput($questionId, $response) {
			$question = $this->questions[$questionId];
			$attr = $question['required'] !== false ? ['required'=>'required'] : [];
			$options = array('' => i18n('select-no-choice'));
			foreach ($question['options'] as $optionKey) {
				$options[$optionKey] = i18nRaw($questionId.'-option-'.$optionKey);
			}							
			return ZForm::GetSelectTag($this->getAnswerName($questionId), $options, $response, ['attr'=>$attr]);
		}

		private function getRate5Input($questionId, $response) {
			$question = $this->questions[$questionId];
			$required = $question['required'] !== false ? 'required' : '';
			$name = $this->getAnswerName($questionId);
			$out .= '<label><input type="radio" class="radio" name="'.$name.'" value="1" '.$required.' />' .i18n('ZSurvey-1-radiobutton') . '</label>';
			$out .= '<label><input type="radio" class="radio" name="'.$name.'" value="2" '.$required.' />' . i18n('ZSurvey-2-radiobutton') . '</label>';
			$out .= '<label><input type="radio" class="radio" name="'.$name.'" value="3" '.$required.' />' . i18n('ZSurvey-3-radiobutton') . '</label>';
			$out .= '<label><input type="radio" class="radio" name="'.$name.'" value="4" '.$required.' />' . i18n('ZSurvey-4-radiobutton') . '</label>';
			$out .= '<label><input type="radio" class="radio" name="'.$name.'" value="5" '.$required.' />' . i18n('ZSurvey-5-radiobutton') . '</label>';
			return $out;
		}
		
		private function getYesNoInput($questionId, $response) {
			$question = $this->questions[$questionId];
			$required = $question['required'] !== false ? 'required' : '';
			$name = $this->getAnswerName($questionId);
			$out = '<label><input type="radio" class="radio" name="'.$name.'" value="yes" '.$required.' />' .i18n('ZSurvey-yes-radiobutton') . '</label>';
			$out .= '<label><input type="radio" class="radio" name="'.$name.'" value="no" '.$required.' />' . i18n('ZSurvey-no-radiobutton') . '</label>';
			return $out;
		}

		private function getRadioInput($questionId, $response) {
			$out = '';
			$count = 1;
			$name = $this->getAnswerName($questionId);
			$question = $this->questions[$questionId];
			$required = $question['required'] !== false ? 'required' : '';
			foreach ($question['options'] as $optionKey) {
				$translate = i18n($questionId.'-option-'.$optionKey);
				$checked = $optionKey == $response ? 'checked="checked"' : '';
				$out .= '<label><input type="radio" class="radio" name="'.$name.'" value="'.$optionKey.'" '.$checked.' '.$required.' /> ' . $translate . '</label>';
				$count ++;
			}							
			return $out;
		}

		private function getTextInput($questionId, $response) {
			$name = $this->getAnswerName($questionId);
			$question = $this->questions[$questionId];
			$required = $question['required'] !== false ? 'required' : '';
			$type = empty($question['text-type']) ? 'text' : $question['text-type'];
			return '<input type="'.$type.'"  name="'.$name.'" value="'.$response.'" '.$required.' />';
		}

		private function getTextAreaInput($questionId, $response) {
			$name = $this->getAnswerName($questionId);
			$question = $this->questions[$questionId];
			$required = $question['required'] !== false ? 'required' : '';
			return '<textarea name="'.$name.'" '.$required.' /></textarea>';
		}

		public function saveFullResponse($surveyData) {
			// $keyHash = static::keysHash($surveyData['answers']);
			// if ($surveyData['checksum'] != $keyHash) {
			// 	throw New Exception('survey data is not valid.');
			// }
			$response = [
				'surveyId' => $this->surveyId,
				'answers' => json_encode($surveyData['answers']),
				'dateStarted' => time(),
				'dateCompleted' => time(),
				'ip' => $_SERVER['REMOTE_ADDR']
			];
			ZDB::Insert('survey_responses', $response);
			ZDB::DieIfError();
			ZRequest::Redirect('thanks.php');
		}

		// public function getCheckSum() {

		// }

		// public static function keysHash($arr) {
		// 	$answerKeys = sort(array_keys($arr));
		// 	return ZAuth::Hash(implode(',', $answerKeys));
		// }

		private function getResponses() {
			return ZDB::GetQuery('SELECT * FROM survey_responses WHERE surveyId = ? AND dateCompleted > 0 ORDER BY dateStarted', [$this->surveyId]);
		}

		private function initializeSurveyStats() {
			$this->surveyStats = [];
			foreach ($this->questions as $questionId => $questionData) {
				$this->surveyStats[$questionId] = $this->getEmptyStats($questionData);
			}
		}

		private function setFilter() {
			$this->filter = [];
			if (!empty($_GET['where'])) {
				$this->filter['where'] = $_GET['where'];		
				$this->filter['is'] = $_GET['is'];
			}
		}

		private function summarizeResponses() {
			$responses = $this->getResponses();
			$this->totalSurveys = 0;
			$this->filteredSurveys = 0;
			foreach ($responses as $response) {
				$this->totalSurveys++;
				$answers = json_decode($response['answers'], true);
				if ($this->shouldFilterOutResponse($answers)) {
					continue;
				}
				$this->filteredSurveys++;
				foreach ($answers as $questionId => $answer) {
					if ($this->isQuestionFreetext($questionId)) {
						$this->surveyStats[$questionId][] = $answer;
					} else {
						$this->surveyStats[$questionId][$answer]++;
					}
				}
			}
		}

		public function getSurveySummary() {
			$this->setFilter();
			$this->initializeSurveyStats();
			$this->summarizeResponses();
			$this->renderResults();
		}

		private function shouldFilterOutResponse($answers) {
			if (empty($this->filter)) {
				return false;
			}
			if ($answers[$this->filter['where']] !== $this->filter['is']) {
				return true;
			}
			return false;
		}

		private function getEmptyStats($questionData) {
			switch ($questionData['type']) {
				case 'yesno':
					return ['yes' => 0, 'no' => 0];
				case 'rate5':
					return ['1' => 0, '2' => 0, '3' => 0, '4' => 0, '5' => 0];
				case 'select':
				case 'radio':
					$out = [];
					foreach ($questionData['options'] as $key) {
						$out[$key] = 0;
					}
					return $out;
				default:
					return [];
					break;
			}
		}

		private function StatTag($stat, $label) {
			?>
				<div class="stat-box">
					<div class="stat"><?=$stat;?></div>
					<div class="label"><?=$label;?></div>
				</div>
			<?php
		}
	
	
		private function getQuestionText($questionId) {
			return i18n($questionId.'-question-text');		
		}
	
		private function getAnswerText($questionId, $answerKey) {
			$question = $this->questions[$questionId];
			switch ($question['type']) {
				case 'rate5':
				case 'yesno':
					return i18n('ZSurvey-'.$answerKey.'-radiobutton');
					break;
				case 'select':
				case 'radio':
					return i18n($questionId . '-option-' . $answerKey);
					break;
				default:
					return $answerKey;
					break;
			}
		}
	
		private function renderResults() {
			if (!empty($this->filter)) {
				?><div class="filter">
					<?=i18n('ZSurvey-filtered-on-message', array(
						'where'=>$this->getQuestionText($this->filter['where']),
						'is'=>$this->getAnswerText($this->filter['where'], $this->filter['is']),
						'total-count' => $this->totalSurveys,
						'filtered-count' => $this->filteredSurveys
					)); ?><br />
					<button onclick="window.location='?';"><?=i18n('zsurvey-clear-filters-button');?></button>
				</div><?php
			}
			?>
				<div class="stat-tags">
					<?php $this->StatTag($this->totalSurveys, i18n('ZSurvey-completed-surveys-label')); ?>
				</div>
				<h1><?=i18n('ZSurvey-answer-summary-by-question');?></h1>
				<div class="zsurvey-summary-table">
					<?php
						foreach ($this->surveyStats as $questionId => $answerData) {
							$this->printQuestionSummary($questionId, $answerData);
						}
					?>
				</div>
			<?php
		}
	
		private function printQuestionSummary($questionId, $answerData) {
			if (!$this->acceptsResponse($questionId)) {
				return;
			}
			?>
				<div class="question">
					<div class="question-text"><?=$this->getQuestionText($questionId);?></div>
					<?php 
						foreach ($answerData as $answerKey => $answerData) { 
							if ($this->isQuestionFreetext($questionId)) {
								?>
									<div class="answer free-text">
										<div class="answer-inner"><?=$answerData?></div>
									</div>
								<?php
							} else {
								$percent = round(100 * $answerData / $this->filteredSurveys);
								$answerText = $this->getAnswerText($questionId, $answerKey);
								$answerText .= ' <span class="count">'.i18n('ZSurvey-answer-count-display', array('percent' => $percent,'count' => $answerData,'total' => $this->filteredSurveys))."</span>";
								?>
								<div class="answer filterable" data-question="<?=$questionId; ?>" data-answer="<?=$answerKey; ?>">
									<div class="percent-bar" style="width:<?=$percent;?>%"></div>
									<div class="answer-inner"><?=$answerText?></div>
									&nbsp;
								</div>
								<?php
							}
						}
					?>
				</div>
			<?php

		}

	}