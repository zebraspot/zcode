<?php
	class ZMessage extends ZBase {
		public static $mailHandler;
		public static $sendInBlueAPIKey;
		public static $mailDomain;
		public static $mailSenderName;
		public static $slackHooks;
		public static $defaultFrom;

		public static function Init() {
			self::Config('message');
			if (empty(static::$defaultFrom)) {
				static::$defaultFrom = 'info@' . (ZCode::$mailDomain ? ZCode::$mailDomain : ZCode::$domain);
			}
			ini_set('sendmail_from', static::$defaultFrom);
		}
		
		public static function SetMailHandler($mh) {
			self::$mailHandler = $mh;
		}		
		
		public static function SendMail($title, $message, $address, $from_mail='info') {
			if (strpos($from_mail, '@') === false) {
				$domain = self::$mailDomain ? self::$mailDomain : (ZCode::$mailDomain ? ZCode::$mailDomain : ZCode::$domain);
				$from_mail .= '@' . $domain;
			}
			$from_name = self::$mailSenderName ? self::$mailSenderName : (ZCode::$mailFromName ? ZCode::$mailFromName : $from_mail);

			if (function_exists('ZMessage_SendMail')) {
				$success = ZMessage_SendMail($title, $message, $address, $from_mail, $from_name);
			} else if (self::$mailHandler) {
				$success = self::$mailHandler->SendMail($title, $message, $address, $from_mail, $from_name);
			} else if (ZAwsSes::$profile) {
				$success = ZAwsSes::SendMail($title, $message, $address, $from_mail);
			} else if (self::$sendInBlueAPIKey) {
				$success = self::SendInBlueMail([
					'subject' => $title,
					'htmlContent' => $message, 
					'to' => [['email' => $address]],
					'sender' => ['email' => $from_mail, 'name' => $from_name]
				]);
			} else {
				$sep = "\r\n";
				$encoding = "utf-8";

				// Preferences for Subject field
				$subject_preferences = array(
					"input-charset" => $encoding,
					"output-charset" => $encoding,
					"line-length" => 76,
					"line-break-chars" => $sep
				);
			
				// Mail header
				$header = "Content-type: text/html; charset=".$encoding." ".$sep;
				$header .= "From: ".$from_name." <".$from_mail."> ".$sep;
				$header = 'Reply-To: ' . $from . ' ' . $sep;
				$header .= 'Sender: ' . $from . ' ' . $sep;
				$header .= "MIME-Version: 1.0 ".$sep;
				$header .= "Content-Transfer-Encoding: 8bit ".$sep;
				$header .= "Date: ".date("r (T)")." ".$sep;
				$header .= iconv_mime_encode("Subject", $title, $subject_preferences);
						
				
				$success = mail($address, $title, wordwrap($message, 70), $header);
			}

			if (!$success) {
				ZLog::Log('Email Failed:', array('address' => $address, 'title' => $title, 'message' => $message, 'headers' => $header));
				ZRequest::Error('SendMail failed');
			} else {
				ZLog::Log('Email Sent:', array('address' => $address, 'title' => $title, 'message' => $message, 'headers' => $header));
			}
			if (function_exists('ZMessage_EmailSent')) {
				ZMessage_EmailSent($address, $title, $message, $header);
			}
			return $success;
		}
		
		public static function PostTextToSlack($hookName, $msg) {
			$json = json_encode(array('text' => $msg));
			self::PostJSONToSlack($hookName, $json);		
		}


		public static function PostJSONToSlack($hookName, $json) {
			if (!self::$slackHooks[$hookName]) {
				ZRequest::Error('Slack hook "'. $hookName . '" does not exist. Please add it in site.conf.php');
			}
			
			$message = array('payload' => $json);

			$c = curl_init(self::$slackHooks[$hookName]);
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($c, CURLOPT_POST, true);
			curl_setopt($c, CURLOPT_POSTFIELDS, $message);
			curl_exec($c);
			curl_close($c);			
		}


		public static function SendInBlueMail($emailData, $verbose = false) {
			if ($verbose) pr($emailData, 'SendInBlue Email Data');
			
			$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', self::$sendInBlueAPIKey);
			$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(new GuzzleHttp\Client(), $config);
			$sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
			$to = [];
			foreach ($emailData['to'] as $toData) {
				$recipient = new \SendinBlue\Client\Model\SendSmtpEmailTo($toData);
				$to[] = $recipient;
			}
			$sendSmtpEmail->setTo($to);
			
			$sender = new \SendinBlue\Client\Model\SendSmtpEmailSender($emailData['sender']);
			$sendSmtpEmail->setSender($sender);

			if (empty($emailData['replyTo'])) {
				$replyTo = new \SendinBlue\Client\Model\SendSmtpEmailReplyTo($emailData['sender']);
			} else {
				$replyTo = new \SendinBlue\Client\Model\SendSmtpEmailReplyTo($emailData['replyTo']);
			}
			$sendSmtpEmail->setReplyTo($replyTo);

			$sendSmtpEmail->setHtmlContent($emailData['htmlContent']);
			if (!empty($emailData['textContent'])) {
				$sendSmtpEmail->setTextContent($emailData['textContent']);
			}
			
			$sendSmtpEmail->setSubject($emailData['subject']);
				
			if (!empty($emailData['bcc'])) {
				$bcc = [];
				foreach ($emailData['bcc'] as $bccData) {
					$bcc[] = new \SendinBlue\Client\Model\SendSmtpEmailBcc($bccData);
				}
				$sendSmtpEmail->setBcc($bcc);	
			}

			if (!empty($emailData['cc'])) {
				$cc = [];
				foreach ($emailData['cc'] as $ccData) {
					$cc[] = new \SendinBlue\Client\Model\SendSmtpEmailCc($ccData);
				}
				$sendSmtpEmail->setCc($cc);	
			}

			if ($verbose) pr($sendSmtpEmail, 'final sendSmtpEmail');
			// $sendSmtpEmail->setAttachment();
			// $sendSmtpEmail->setHeaders();
			// $sendSmtpEmail->setTemplateId();
			// $sendSmtpEmail->setParams();
			// $sendSmtpEmail->setTags();

			try {
				$result = $apiInstance->sendTransacEmail($sendSmtpEmail);
				ZLog::Log('SendInBlue Email Sent', $result);
				return true;
			} catch (Exception $e) {
				ZLog::Log('Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage());
				return false;
			}
		}


	}