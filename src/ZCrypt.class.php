<?php
	class ZCrypt {
		public static $salt = 'j5KApQRGDUN55Gp2fDwQ42lZ9RlpGY0zt';
		
		public static function token($seed = null) {
			if ($seed == null) {
				$seed = 'r' . microtime() . rand(0, 9999);	
			}
			$code = static::hash($seed, 56);
			$checksum = static::hash($code, 8);
			return $code . $checksum;
		}

		public static function hash($seed, $length) {
			$hash = hash_hmac ( 'sha512' , $seed , static::$salt , true );
			$hash = substr(base64_encode($hash),0,$length);
			$hash = str_replace('+', '-', $hash);
			$hash = str_replace('/', '.', $hash);
			return $hash;
		}

		public static function checkToken($token = null) {
			if (strlen($token) != 64) {
				return false;
			}
			$code = substr($token, 0, 56);
			$tokenChecksum = substr($token, 56, 8);
			$codeChecksum = static::hash($code, 8);
			return $tokenChecksum === $codeChecksum;
		}

	}
	
	