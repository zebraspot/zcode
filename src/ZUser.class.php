<?php
	class ZUser extends ZTable {
		public static $recordValues = [];

		public static $setCreator = true;
		public static $tableName = 'users';
		public static $baseOptionsForSelect = [];
		public static $loggedIn;
		private $groups;
		public static $manyToManyJoins = ['usergroups' => [
			'joinToClass' => 'ZUserGroup'
		]];

		function __construct($data = []) {
			return parent::__construct($data);
		}

		public static function SetLoggedInUser($userData) {
			static::$loggedIn = new static($userData);
		}

		public static function SetTableName($tableName) {
			static::$tableName = $tableName;
		}

		public function getDisplayName() {
			$name = trim($this->data['firstname'] . ' ' . $this->data['lastname']);
			if (empty($name)) {
				$name = $this->data['email'];
			}
			return $name;
		}

		public function GetGroups() {
			if ($this->groups == null) {
				$this->groups = ZUserGroup::GetAllForUser($this->id);
			}
			return $this->groups;
		}
	}

	ZUser::Init();