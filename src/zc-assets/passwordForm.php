<style>
	.zcode-site-password-form {
		width: 340px;
		position: absolute;
		top: 170px;
		left: calc(50% - 170px);
		background: white;
		border: 3px solid #980000;
		z-index: 1000;
		padding: 16px 30px;
		box-shadow: 9px 10px 17px rgba(0,0,0,0.4);
	}
</style>
<div class='zcode-site-password-form'>
	Please enter the password:
	<form method="post">
		<input name="zcode-site-access" type="password" />
		<input type="submit" name="submit" value="Go..." />
	</form>
</div>