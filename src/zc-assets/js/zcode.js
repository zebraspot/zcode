// zcode js
const ZUtils = {
	striptags: function (str) {
		return str.replace(/(<([^>]+)>)/gi, "");
	},

	post: function (url, data, doneFunction, failFunction) {
		if (!doneFunction) {
			doneFunction = (data, status) => {
				console.log('done', data, status);
			};
		}

		if (!failFunction) {
			failFunction = (err) => {
				alert(`post failed`);
				console.log('fail: ', err.statusText);
			};
		}

		$.ajax({
			url: url,
			method: "POST",
			data: data,
			dataType: "json"
		}).done(doneFunction).fail(failFunction);

	},

	copyToClipboard: (str) => {
		const el = document.createElement('textarea');
		el.value = str;
		document.body.appendChild(el);
		el.select();
		document.execCommand('copy');
		document.body.removeChild(el);
	},

	setupTinyMCE: function (callback) {
		if (window.localStorage[zcode.pageType]) {
			savedHeight = window.localStorage[zcode.pageType] + 'px';
			$('textarea.rich').css({ height: savedHeight });
		}

		let tinyParams = {
			selector: 'textarea.rich',
			menubar: false,
			plugins: 'lists',
			toolbar: 'styleselect | bold italic underline strikethrough | bullist numlist',
			setup: function (editor) {
				editor.on('Dirty', function (e) {
					$(e.target.iframeElement).addClass('dirty');
				});
				editor.on('init', function () {
					$(editor.getWin()).bind('resize', debounce(() => {
						const height = $('.tox.tox-tinymce')[0].offsetHeight;
						window.localStorage[zcode.pageType] = height;
					}, 1000));
				});
				editor.on('keydown', function (e) {
					if ((navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.code == 'KeyS') {
						if ($('form.override-save').length) {
							e.preventDefault();
							$('form.override-save').submit();
						}
					}
				});
			}
		};

		if (zcode.tinyParams) {
			tinyParams = Object.assign(tinyParams, zcode.tinyParams);
		}
		tinymce.init(tinyParams);
	},

	goWhence: function () {
		const whence = $('#whence').val();
		if (whence) {
			window.location = whence;
		}
		return false;
	},

	insertAtCursor:	function(myField, myValue) {
		//IE support
		if (document.selection) {
			myField.focus();
			sel = document.selection.createRange();
			sel.text = myValue;
		}
		//MOZILLA and others
		else if (myField.selectionStart || myField.selectionStart == '0') {
			var startPos = myField.selectionStart;
			var endPos = myField.selectionEnd;
			myField.value = myField.value.substring(0, startPos)
				+ myValue
				+ myField.value.substring(endPos, myField.value.length);
		} else {
			myField.value += myValue;
		}
	},

	fullScreen: function(selector) {
		let elem = document.querySelector(selector);
		// console.log(elem);
		if (!document.fullscreenElement) {
			elem.requestFullscreen().catch(err => {
				alert(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
			});
		} else {
			document.exitFullscreen();
		}
	}

}

$(() => {
	$('.zform-tab-tags').on('click', function () {
		$tabHolder = $(this).parents('.zform-tabs');
		$tabHolder.find('.current').removeClass('current');
		$(this).addClass('current');
		$('#' + this.dataset.tab).addClass('current');
	});

	$('.form-element input[type="text"], .form-element text-area').on('change', function() {
		validateTextInput(this);
	});

	$('form.zform').on('submit', function() {
		var valid = true;
		$(this).find('.form-element input[type="text"], .form-element text-area').each(function() {
			valid = valid && validateTextInput(this, true);
		});
		// return false;
		return valid;
	});

	function validateTextInput(el, report) {
		if (el.dataset.stringMask) {
			val = $(el).val();
			const regex = new RegExp(el.dataset.stringMask);
			if (val.match(regex)) {
				el.setCustomValidity('');
				return true;
			}
			el.setCustomValidity(el.dataset.stringMaskFailed ? el.dataset.stringMaskFailed : 'Does not meet string requirements.');
			if (report) {
				el.reportValidity();
			}
			return false;
		}
		return true;
	}


});

var aa;

function debounce(func, time) {
	var time = time || 100;
	var timer;
	return function (event) {
		if (timer) { clearTimeout(timer); }
		timer = setTimeout(func, time, event);
	};
}

