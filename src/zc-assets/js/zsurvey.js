$(() => {
	$('.zsurvey-summary-table .answer.filterable').click((e) => {
		window.location = '?where='+e.currentTarget.dataset.question+'&is='+e.currentTarget.dataset.answer;
	});
	ZSurveySetUpForm();
});

function ZSurveySetUpForm() {
	function ZSurveyUpdateForm(noAnimate) {
		for (const questionId in ZSurveyData) {
			if (ZSurveyData[questionId]['if']) {
				if (Array.isArray(ZSurveyData[questionId]['if'])) {
					ZSurveyData[questionId]['if'].forEach((ifData) => {
						ZSurveyProcessIf(questionId, ifData, noAnimate);
					});
					
				} else {
					ZSurveyProcessIf(questionId, ZSurveyData[questionId]['if'], noAnimate);
				}
				
			}
		}
	}
	
	function ZSurveyProcessIf(questionId, ifData, noAnimate) {
		const questionValue = ZSurveyGetQuestionValue(questionId);
		noAnimate = noAnimate === true;
		console.log(noAnimate);
		if (questionValue == ifData['answer']) {
			$qb = $('#question-block-' + ifData['show']);
			if ($qb.height() == 0) {
				$qb.css('height', 'auto');
				const autoHeight = $qb.height();
				$qb.height(0).animate({height: autoHeight}, 700);
			}
		} else {
			if (noAnimate) {
				$('#question-block-' + ifData['show']).css('height','0px');
			} else {
				$('#question-block-' + ifData['show']).animate({height: '0px'}, 700);;
			}

		}
	}
	
	function ZSurveyGetQuestionValue(questionId) {
		if (['radio', 'yesno', 'rate5'].indexOf(ZSurveyData[questionId].type) >= 0) {
			return $('#question-block-' + questionId + ' input:checked').val();
		}
		return $('#question-block-' + questionId + ' input').val();
	}

	for (const questionId in ZSurveyData) {
		if (ZSurveyData[questionId]['if']) {
			$('#question-block-' + questionId + ' input').on('change', ZSurveyUpdateForm);
		}
	}
	
	ZSurveyUpdateForm(true);

}
