$().ready((e) => {
	zCode_BindAuthoring();
})

function zCode_BindAuthoring() {
	$('.i18n-phrase').click((event) => {
		var clickshield_mousedown = false;
		if (event.shiftKey) {
			event.stopPropagation();
			$.ajax({ type: "POST",   
				url: "/zc-assets/i18n/authoringForm.php",
				data: {
					key: event.currentTarget.dataset.key,
					insertKeys: event.currentTarget.dataset.insertkeys
				 },
				 success : function(text)
				 {
					 $('body').append(text);
					 $('.i18n-authoring-form').mousedown((e) => {e.stopPropagation();}).mouseup((e) => {e.stopPropagation();});
					 $('.i18n-clickshield').mousedown((event)=>{
						clickshield_mousedown = true;
						setTimeout(()=>{clickshield_mousedown = false}, 1000);
					 }).mouseup((event) => {
						clickshield_mousedown && $('.i18n-clickshield').remove();
					 });
					 $('span.i18n-key').click(function(event) {
						let key = '{$' + $(this).text() + '}';
						$('#i18n-translation').insertAtCaret(key);
					 });

					$('#i18n-translation').focus().select();
				 }
			});
			return false;
		}
	}).mouseup((event) => {
		if (event.shiftKey) {
			event.stopPropagation();
			return false;
		}
	});
	document.addEventListener("keydown", function(e) {
		if ($('#i18n-translation').length) {
			if ((window.navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey) && e.code == 'KeyS') {
				e.preventDefault();
				zCode_saveTranslation();
				return false;
			} else if (e.code == 'Escape') {
				e.preventDefault();
				$('.i18n-clickshield').remove();
				return false;
			}
		}
	}, false);
}

function zCode_saveTranslation() {
	var params = {lang: zcode.lang};
	$('.i18n-authoring-form').find('input, textarea').each((i, element) => {
		params[element.name] = $(element).val();
	});

	$.ajax({ type: "POST",   
		url: "/zc-assets/i18n/saveTranslation.php",
		data: params,
	     success : function(text)
	     {
		 	$('.i18n-clickshield').remove();
	     }
	});
}

function i18n(translationKey, inserts) {
	let translation = zcode.i18n[translationKey];
	if (translation) {
		if (inserts) {
			for (let key in inserts) {
				translation = translation.replace('{$' + key + '}', inserts[key]);
			}
		}
		return translation;
	}
	let untranslatedPlaceholder = '<span class="i18n-error" data-keys="' + JSON.stringify(Object.keys($inserts)) + '" >' + translationKey + '</span>';
	ZLogI18nError(untranslatedPlaceholder);
	return untranslatedPlaceholder;
}

jQuery.fn.extend({
	insertAtCaret: function(myValue){
	  return this.each(function(i) {
			if (document.selection) {
				//For browsers like Internet Explorer
				this.focus();
				var sel = document.selection.createRange();
				sel.text = myValue;
				this.focus();
			} else if (this.selectionStart || this.selectionStart == '0') {
				//For browsers like Firefox and Webkit based
				var startPos = this.selectionStart;
				var endPos = this.selectionEnd;
				var scrollTop = this.scrollTop;
				this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
				this.focus();
				this.selectionStart = startPos + myValue.length;
				this.selectionEnd = startPos + myValue.length;
				this.scrollTop = scrollTop;
			} else {
				this.value += myValue;
				this.focus();
			}
		});
	}
});