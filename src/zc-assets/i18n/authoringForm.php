<?php
	require('site.php');

	if (!ZI18n::$on || !ZI18n::$authorMode) {
		die;
	}

?>
<div class="i18n-clickshield">
	<div class="i18n-authoring-form">
		<h3>Enter Translation (<?=ZI18n::GetLang(); ?>):</h3>
		<?php
			$translationFile = ZI18n::GetTranslationFileURL();
			if (!file_exists($translationFile)) {
				echo '<div class="error">File: ' . $translationFile . ' does not exist.  Please create it!</div>';
			} else if (!is_writable($translationFile)) {
				echo '<div class="error">File: ' . $translationFile . ' is not writable.  Please grant the php user permissions to write to this file.</div>';
			}
		?>
		<div>
			<b>Translation Key: </b><br />
			<input name="key" disabled="disabled" value="<?=$_POST['key'];?>" />
		</div>
		<?php if (!empty($_POST['insertKeys'])) {
			?>
				<div>
					<b>Value Insertion Keys: </b><br />
					<?php
						$keys = explode(',',$_POST['insertKeys']);
						foreach ($keys as $key) {
							?><span class="i18n-key"><?=$key;?></span><?php
						}
					?>
					
				</div>
			<?php
		} ?>
		<div>
			<b>Translation: </b><br />
			<textarea id="i18n-translation" name="translation"><?=ZI18n::GetRawPhrase($_POST['key']);?></textarea>
		</div>
		<div>
			<button onclick="zCode_saveTranslation();">Save Translation</button>
			<button onclick="$('.i18n-clickshield').remove();">cancel</button>
		</div>
	</div>
</div>