<?php
	require('site.php');

	if (!ZI18n::$on || !ZI18n::$authorMode) {
		die('Translation Not Enabled');
	}	

	$translationFile = ZI18n::GetTranslationFileURL();
	$translations = file_get_contents($translationFile);
	$translations = json_decode($translations, true);
	$translations[$_POST['key']] = $_POST['translation'];

	$json = json_encode($translations);
	$json = str_replace('","', "\",\n\"", $json);
	file_put_contents($translationFile, $json);
