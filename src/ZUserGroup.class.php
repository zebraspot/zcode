<?php
	class ZUserGroup extends ZTable {
		public static $recordValues = [];

		public static $setCreator = true;
		public static $tableName = 'usergroups';
		public static $restrictable = true;
		public static $baseOptionsForSelect = ['0' => '[ Everyone ]', '-1' => '[ Owner Only ]'];
		public static $manyToManyJoins = ['users' => [
			'joinTable' => 'users_usergroups',
			'joinToClass' => 'ZUser'
		]];

		function save($updateOnly = false) {
			parent::save($updateOnly);
			if ($this->id != $this->data['restrict_to']) {
				$this->data['restrict_to'] = $this->id;
				parent::save(true);
			}
		}

		public static function GetAllForUser($userId) {
			$out = [];
			$groups = ZDB::GetQuery('SELECT * FROM users_usergroups WHERE user_id = ?;', [$userId]);
			foreach ($groups as $group) {
				$out[] = $group['usergroup_id'];
			}
			return $out;
		}

		public function getMemberIds() {
			$out = [];
			$users = ZDB::GetQuery('SELECT * FROM users_usergroups WHERE usergroup_id = ?;', [$this->id]);
			foreach ($users as $user) {
				$out[] = $user['user_id'];
			}
			return $out;
		}

		public function SaveMembers($memberIds) {
			$this->PersistManyToMany($memberIds, 'users');
		}

	}

	ZUserGroup::Init();