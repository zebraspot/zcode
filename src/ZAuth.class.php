<?php

	class ZAuth extends ZBase {
		protected static $salt = 'fgs5idfnwebnkssrnmsdf';
		protected static $userTable = 'admin_members';
		public static $automatedTests = false;
		public static $admin = false;
		public static $loggedInUser;
		public static $adminEmail = 'simon@zebraspot.com';
		public static $authFolder = '/';
		public static $debugLogIn = false;
		public static $sitePassword = 'zzcode';
	
		public static function Init() {
			self::Config('auth');
			ZUser::SetTableName(static::$userTable);
			if (@$_GET['automatedtesting'] == '1') {
				ZAuth::$automatedTests = true;
				ZAuth::OverrideAdminEmail();
			}
		}		

		public static function CheckUser($redirect = true) {
			global $mobile;
			ZAuth::$admin = false;
			if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == $_SERVER['REMOTE_ADDR']) {
				ZAuth::$automatedTests = true;
				ZAuth::OverrideAdminEmail();
			}

			if (@$_POST['logout']) {
				// Logging Out
				ZAuth::Logout();
				header("Location: /");die;
	
			} else if (isset($_POST['username'])) {
				$_POST['username'] = trim($_POST['username']);
				// Logging in
				ZLog::log("Using POST for Login", $_POST);
				$_SESSION['username'] = $_POST['username'];
				$h = ZAuth::Hash($_POST['password']);
				$hh = ZAuth::Hash($h);
				$_SESSION['password'] = $hh;
				if (isset($_POST['rememberme'])) {
					ZLog::log("setting rememberme to true", '');
					static::saveLoginCookies($_POST['username'], $hh);
				}
			} else if (empty($_SESSION['password']) && isset($_COOKIE['username'])) {
				// Logging in
				ZLog::log("Using Cookie for Login", '');
				$_SESSION['username'] = $_COOKIE['username'];
				$_SESSION['password'] = $_COOKIE['phh'];
			}

			if (!empty($_SESSION['password'])) {
				$users = ZDB::GetQuery(
					'SELECT * FROM '.ZAuth::$userTable.' WHERE status = '.NORMAL_STATUS.' AND email LIKE ?',
					array($_SESSION['username'])
				);
				if (!empty($users)) {
					ZAuth::$loggedInUser = $users[0];
					self::pr(self::$loggedInUser, 'logged in user');
					$hh = ZAuth::Hash(ZAuth::$loggedInUser['password']);
					self::pr($hh, 'logged in user');
					if ($hh != $_SESSION['password']) {
						ZAuth::$loggedInUser = null;
					} else {
						ZUser::SetLoggedInUser(ZAuth::$loggedInUser);
						if ($_COOKIE['username']) {
							static::saveLoginCookies($_SESSION['username'], $hh);
						}
					}
				}
				self::pr($users);
			}
			if (empty(ZAuth::$loggedInUser)) {
				if ($redirect) {
					if (!empty($_SESSION['username'])) {
						ZRequest::SetFlash(i18n('ZC-login-failed'));
					}
					if (!self::$debugLogIn) {
						ZRequest::SetSessionVariable('redirectUrlAfterLogin', $_SERVER['REQUEST_URI']);
						if (!empty($_POST)) {
							unset($_POST['username']);
							unset($_POST['password']);
							if (!empty($_POST)) {
								ZLog::log("FailedLoginPostParams", json_encode($_POST), 'parse', true);
								ZRequest::SetSessionVariable('failedLoginPostParams', $_POST);
							}
						}
						ZRequest::Redirect(ZAuth::$authFolder . 'login.php');
					} else {
						ZRequest::Error(i18n('ZC-login-failed'));
					}
				}
				return false;
			}
			self::pr('Log in Successful!!');
			if (isset(ZAuth::$loggedInUser['admin']) && ZAuth::$loggedInUser['admin'] == 1) {
				ZAuth::$admin = true;
			}
	
// 			DiyodeMember::AfterLogin();
			return true;
		}

		private static function saveLoginCookies($username, $hh) {
			$date_of_expiry = time() + 60 * 60 * 24 * 30 ;
			setcookie('username', $username, $date_of_expiry, '/');
			setcookie('phh', $hh, $date_of_expiry, '/');
		}
		
		public static function pr($val, $label = "Login Debug") {
			if (self::$debugLogIn) {
				ZUtils::pr($val, $label);
			}	
		}
		
		public static function Hash($str) {
			$out = md5(ZAuth::$salt . $str);
			self::pr('Hash for "'.ZAuth::$salt . $str.'"', $out);
			return $out;
		}
	
		public static function LongHash($str) {
			return md5(ZAuth::$salt . $str) . md5($str . ZAuth::$salt);
		}
	
		public static function CheckPassword() {
			if (self::Hash(self::$sitePassword) == $_COOKIE['sph']){
				return true;
			} else if (!empty($_POST['zcode-site-access'])) {
				if ($_POST['zcode-site-access'] == self::$sitePassword) {
					$date_of_expiry = time() + 60 * 60 * 24 * 30 ;
					setcookie('sph', self::Hash(self::$sitePassword), $date_of_expiry, '/');
					return true;
				}
				ZRequest::SetFlash('The Password was incorrect');
			}
			ZRequest::Notice(file_get_contents(__DIR__ . '/zc-assets/passwordForm.php'));
		}

		public static function CheckAdmin($redirect = true) {
			$loggedIn = ZAuth::CheckUser($redirect);
			ZAuth::$admin = false;
			if ($loggedIn) {
				if (ZAuth::$loggedInUser['admin'] != 1) {
					if ($redirect) {
						ZRequest::Forbidden(i18n('ZC-page-access-forbidden'));
					}
					return false;
				}
				ZAuth::$admin = true;
				return true;
			}
			return false;
		}
	
		private static function LogInUser($user) {
			$date_of_expiry = time() + 60 * 60 * 24 * 30 ;
			setcookie('username', $user['email'], $date_of_expiry, '/');
			setcookie('phh', self::Hash($user['password']), $date_of_expiry, '/');
		}			
	
		public static function Logout() {
			$_SESSION['username'] = $_SESSION['password'] = '';
			$date_of_expiry = time() - 3600;
			setcookie('username', null, $date_of_expiry, '/');
			setcookie('phh', null, $date_of_expiry, '/');
			unset($_COOKIE['username']);
			header('Location: '.ZCode::$root);
		}
	
	
	
		public static function GetPhotoUrl($memberId) {
			$photos = ZDB::GetQuery("SELECT * FROM admin_member_photos WHERE current = 1 AND member_id = $memberId");
			if (!empty($photos)) {
				return '/directory/photos/' . $photos[0]['filename'];
			}
			return '/directory/noPhoto.jpg';
		}
	
	
		public static function SaveMugshot($memberName, $memberId) {
			global $SitePath;
			ZLog::ErrorIfEmpty('$memberName', $memberName);
			ZLog::ErrorIfEmpty('$memberId', $memberId);
			if (!empty($_FILES) && $_FILES['mugshot']['error'] == 0) {
				$ccName = ZUtils::ToCamelCase($memberName) . '_' . date('Y-m-d_G-i');
				$photo = ZCode::$path.'directory/photos/'.$ccName.'.jpg';
				$largePhoto = ZCode::$path.'directory/photos/'.$ccName.'_Orig.jpg';
				//require_once('ZGD.class.php');
				ZGD::ScaleImage($_FILES['mugshot']['tmp_name'], $largePhoto, 1024,1024);
				ZGD::MakeThumb($_FILES['mugshot']['tmp_name'], $photo,400,533);
				ZDB::GetQuery("UPDATE admin_member_photos SET current = 0 WHERE current = 1 AND member_id = ?", array($memberId));
				ZDB::GetQuery(
					"INSERT INTO admin_member_photos (member_id, filename, timestamp, current) VALUES (?, ?, ?, 1);",
					array($memberId, $ccName.'.jpg', time())
				);
			} else if ($_FILES['mugshot']['error'] != 4) {
				$error = i18n('ZC-error-uploading-user-photo');
				ZLog::error('Image Process Error!', $error, 'parse');
				return $error;
			}
		}
	
		public static function OverrideAdminEmail() {
// 			ZAuth::$adminEmail = DiyodeMember::GetSetting('admin_email_for_tests');
		}

		public static function processPasswordReset() {
			$p1 = $_POST['password1'];
			$p2 = $_POST['password2'];
			if ($p1 != $p2) {
				ZRequest::SetFlash(i18n('ZC-passwords-different-message'));
				return false;
			}
			if (strlen($p1) < 4) {
				ZRequest::SetFlash(i18n('ZC-password-too-short-message'));
				return false;
			}
			$userId = $_POST['userid'];
			$resetCode = $_POST['rc'];
			if (empty($resetCode) || strlen($resetCode) != 64) {
				ZRequest::Notice(i18n('ZC-password-reset-code-not-valid'));
			}
			$users = ZDB::GetQuery('SELECT * FROM '.ZAuth::$userTable.' WHERE id = ? AND resetCode = ?', array($userId, $resetCode));
			self::pr($users);
			if (empty($users)) {
				ZRequest::Notice(i18n('ZC-password-reset-code-not-valid'));
			} else {
				$user = $users[0];
				$userId = $user['id'];
				$passwordHash = ZAuth::Hash($p1);
				ZDB::GetQuery(
					'UPDATE '.ZAuth::$userTable.' SET password = ?, resetCode = ?, status = ? WHERE id LIKE ?',
					array($passwordHash, '', NORMAL_STATUS, $userId)
				);
				self::LogInUser(ZDB::GetRecord(ZAuth::$userTable, 'id', $userId));
				ZRequest::SetFlash(i18n('ZC-new-password-saved'));
				return true;
			}
			
		}
		
		public static function processActivation() {
			$loginCode = $_GET['lc'];
			if (empty($loginCode) || strlen($loginCode) != 64) {
				ZRequest::Notice(i18n('ZC-activation-code-not-valid'));
			}
			$user = self::GetUserFromLoginCode($loginCode);
			self::pr($user);
			if (empty($user)) {
				ZRequest::Notice(i18n('ZC-activation-code-not-valid'));
			} else {
				$userId = $user['id'];
				$passwordHash = $user['password'];
				ZDB::GetQuery(
					'UPDATE '.ZAuth::$userTable.' SET loginCode = ?, status = ? WHERE status = ? AND id LIKE ?',
					array('', NORMAL_STATUS, DRAFT_STATUS, $userId)
				);

				$hh = ZAuth::Hash($user['password']);
				$date_of_expiry = time() + 60 * 60 * 24 * 30 ;
				setcookie('username', $user['email'], $date_of_expiry, '/');
				setcookie('phh', $hh, $date_of_expiry, '/');
				$_SESSION['username'] = $user['email'];
				$_SESSION['password'] = $hh;
				
				ZRequest::SetFlash(i18n('ZC-activation-complete-message'));
			}
			
		}
		
		public static function SendResetCode($email) {
			$users = ZDB::GetQuery('SELECT * FROM '.ZAuth::$userTable.' WHERE (status = '.NORMAL_STATUS.' OR status = '.DRAFT_STATUS.') AND email LIKE ?', array($email));
			if (empty($users)) {
				$message = i18n('ZC-reset-password-email-address-not-found-message', array('email' => $email));
			} else {
				$user = $users[0];
				$userId = $user['id'];
				$resetCode = ZAuth::LongHash(microtime().'456hbsdfwerytvdfgn');
				ZDB::GetQuery(
					'UPDATE '.ZAuth::$userTable.' SET resetCode = ? WHERE id LIKE ?', 
					array($resetCode, $userId)
				);
				if (function_exists('ZAuthSendResetCodeEmail')) {
					ZAuthSendResetCodeEmail($user, $resetCode);
				} else {
					ZAuth::sendResetCodeEmail($user, $resetCode);
				}
				$message = i18nPTag('ZC-reset-password-email-sent-message');
				
				
			}
			return $message;
		}

		public static function createUser($user) {
			$user['password'] = ZAuth::Hash($user['password']);
			$user['loginCode'] = ZAuth::LongHash(microtime().'534g56sdsghd');
			$user['status'] = DRAFT_STATUS;
			
			$user = ZDB::Insert(ZAuth::$userTable, $user);
			ZRequest::DieIfError();
			$userId = $user['id'];
			
			if (function_exists('ZAuthSendLoginCodeEmail')) {
				ZAuthSendLoginCodeEmail($user, $loginCode);
			} else {
				ZAuth::sendLoginCodeEmail($user);
			}
			$message = i18nPTag('ZC-activation-email-sent-message');
			return $message;
		}

		public static function sendResetCodeEmail($user, $resetCode) {
			$message = i18nRaw('ZC-reset-password-emailbody', array('resetLink' => ZAuth::$authFolder . 'passwordReset.php?rc='.$resetCode));
			$subject = i18nRaw('ZC-reset-password-emailsubject', array('siteName' => ZCode::$siteName));
			ZMessage::SendMail($subject, $message, $user['email']);
		}
		
		public static function sendLoginCodeEmail($user) {
			$message = i18nRaw('ZC-activate-account-emailbody', array('activateLink' => ZAuth::$authFolder . 'activate.php?lc='.$user['loginCode']));
			$subject = i18nRaw('ZC-activate-account-emailsubject', array('siteName' => ZCode::$siteName));
			ZMessage::SendMail($subject, $message, $user['email']);
		}
		
		public static function GetUserFromResetCode($resetCode) {
			if (empty($resetCode) || strlen($resetCode) != 64) {
				ZRequest::Error(i18n('ZC-invalid-resetcode-massage', array('resetCode' => $resetCode)));
			}
		
			$users = ZDB::GetQuery(
				'SELECT * FROM '.ZAuth::$userTable.' WHERE resetCode LIKE ?', 
				array($resetCode)
			);
			
			if (empty($users)) {
				ZRequest::Error(i18n('ZC-invalid-resetcode-massage', array('resetCode' => $resetCode)));
			}
		
			return $users[0];
		}
		
		public static function GetUserFromLoginCode($loginCode) {
			if (empty($loginCode) || strlen($loginCode) != 64) {
				ZRequest::Error(i18n('ZC-invalid-activation-code-massage', array('loginCode' => $loginCode)));
			}
		
			$users = ZDB::GetQuery(
				'SELECT * FROM '.ZAuth::$userTable.' WHERE status = ? AND loginCode LIKE ?', 
				array(DRAFT_STATUS, $loginCode)
			);
			
			if (empty($users)) {
				ZRequest::Error(i18n('ZC-invalid-activation-code-massage', array('loginCode' => $loginCode)));
			}
		
			return $users[0];
		}

		public static function LoginFormPersistPosts() {
			$paramsToPost = ZRequest::GetSessionVariable('failedLoginPostParams');
			// pr($paramsToPost);
			if (!empty($paramsToPost)) {
				unset($paramsToPost['username']);
				unset($paramsToPost['password']);
				ZForm::PrintHiddenInputs($paramsToPost);
			}
		}
		
	}
	
