<?php
	class ZRequest extends ZBase {
		protected static $header = '_header.php';
		protected static $footer = '_footer.php';
		protected static $sourceIPs = array();
		protected static $serverIP = '';
		protected static $renderingHTML = false;
		protected static $renderingPlainText = false;
		public static $shouldRenderAsHTML = null;
		public static $headerSnippet = '';
		public static $bodyClass = '';
		public static $useJQuery = true;
		public static $useHighCharts = false;
		public static $useCssReset = true;
		public static $tinyMCE5Key = false;
		public static $tinyMCECallback = false;
		private static $pageTitle = null;
		private static $jqueryAdded = false;
		private static $headElements = array();
		public static $webRoot = '';
		public static $validViews = [];
		private static $scriptFiles = ['global' => [],'site' => [],'page' => []];
		private static $cssFiles = ['global' => [],'site' => [],'page' => []];
		private static $currentView;
		public static $assetVersion;
		public static $breadcrumbs =[];

		public static function Init() {
			self::Config('request');
			$view = @$_SESSION['ZRequest-currentView'];
			if (!empty($view) && in_array($view, self::$validViews)) {
				self::$currentView = $view;
			}
		}

		public static function SetView($view) {
			self::checkIsValidView($view);
			$_SESSION['ZRequest-currentView'] = self::$currentView = $view;
		}

		public static function IsView($view) {
			self::checkIsValidView($view);
			return self::$currentView == $view;
		}
		
		public static function RequireView($view) {
			if (!self::IsView($view)) {
				ZRequest::SetFlash(i18n($view.'-view-not-authorized-message'));
				ZRequest::Redirect('/');
			}
		}
		
		public static function checkIsValidView($view) {
			if (empty(self::$validViews)) {
				die('Error: ZRequest $validViews is not set.  Please specify a list of validViews in site.conf.');
			}
			if (!in_array($view, self::$validViews)) {
				die('Error: View "'.$view.'" is not valid.  Use one of ["'.implode('", "', self::$validViews).'"]');
			}
		}
		
		public static function getView() {
			return self::$currentView;
		}

		public static function Header($pageTitle = false, $bodyClass = false) {
			if ($pageTitle) {
				self::SetPageTitle($pageTitle);	
			}
			if ($bodyClass) {
				self::SetBodyClass($bodyClass);
			}
			include(ZCode::$path.self::$header);
		}

		public static function PageHeading() {
			echo static::PrintBreadcrumbs();
			echo self::GetPageTitle('h1', 'page-heading');
		}
		
		public static function Footer() {
			include(ZCode::$path.self::$footer);
		}

		public static function setWebRoot($newWebRoot) {
			self::$webRoot = $newWebRoot;
		}
		
		public static function PrintHeadElements() {
			$verStr = self::$assetVersion ? ('?v='.self::$assetVersion) : '';
			self::$useJQuery && self::PrintJQuery();
			self::$useHighCharts && self::PrintHighCharts();
			self::$useCssReset && self::PrintCssReset();
			self::$tinyMCE5Key && self::PrintTinyMCE();
			self::PrintZCodeSupport();
			foreach (self::$cssFiles as $fs) {
				foreach ($fs as $f) {
					?><link rel="stylesheet" href="<?=$f.$verStr;?>" />
					<?php
				}
			}
			foreach (self::$scriptFiles as $fs) {
				foreach ($fs as $f) {
					?><script language="javascript" src="<?=$f[0].$verStr;?>" <?=ZUtils::GetAttributesString($f[1]);?>></script>
					<?php
				}
			}
			
			ZI18n::PrintI18nAuthoring();

			$zCodeForJS = ZCode::GetZCodeForJS();
			?><script>zcode = <?=json_encode($zCodeForJS); ?>;</script><?php
			
			echo implode("\r", self::$headElements);
		}
		
		public static function AddHeadElements($html) {
			self::$headElements[] = $html;
		}

		public static function AddScript($scriptFile, $where='page', $attr = null) {
			self::$scriptFiles[$where][] = [$scriptFile, $attr];
		}

		public static function AddCssFile($cssFile, $where='page') {
			self::$cssFiles[$where][] = $cssFile;
		}

		public static function PrintJQuery() {
			if (!self::$jqueryAdded) {
				self::AddScript('https://code.jquery.com/jquery-1.12.4.js', 'global');
				self::AddScript('https://code.jquery.com/ui/1.12.1/jquery-ui.js', 'global');
				self::AddCssFile('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', 'global');
				self::$jqueryAdded = true;
			}	
		}
		
		public static function PrintHighCharts() {
			self::AddScript('https://code.highcharts.com/highcharts.js', 'global');
			self::AddScript('https://code.highcharts.com/modules/exporting.js', 'global');
			self::AddScript('https://code.highcharts.com/modules/export-data.js', 'global');
		}
		
		public static function PrintCssReset() {
			self::AddCssFile('/zc-assets/css/reset.css', 'global');
		}

		public static function PrintZCodeSupport() {
			self::AddScript('/zc-assets/js/zcode.js', 'global');
		}

		public static function PrintTinyMCE() {
			// See ZCode/docs/ZRequest for configuring TinyMCE
			self::AddScript('https://cdn.tiny.cloud/1/'.self::$tinyMCE5Key.'/tinymce/5/tinymce.min.js', 'global', ['referrerpolicy' => 'origin']);
			self::AddHeadElements('<script>$(()=> {ZUtils.setupTinyMCE('.self::$tinyMCECallback.');});</script>');
		  }
		
		//   toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent'

		public static function PrintHeaderSnippet() {
			echo self::$headerSnippet;
		}

		public static function LimitToSource() {
			if (array_search($_SERVER['REMOTE_ADDR'], self::$sourceIPs) === false) {
				die('Authentication Failed: <!-- '.$_SERVER['REMOTE_ADDR'].'--> SRD_Check');
			}
			if ($_SERVER['REQUEST_SCHEME'] != 'https') {
				die('Authentication Failed: SIHS_Check');
			}
		}

		public static function LimitToLocalhost() {
			global $beta;
			if (!($beta || $_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == self::$serverIP)) {
				die ('401 ' . $_SERVER['REMOTE_ADDR']);
			}

		}

		public static function LimitToBeta() {
			global $beta;
			if (!$beta) {
				die ('401 ' . $_SERVER['REMOTE_ADDR']);
			}
		}

		public static function EnforceHttps() {
			global $debug;
			if ($debug) {
				return;
			}
			if (strtolower($_SERVER['REQUEST_SCHEME']) != 'https') {
				$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
				header("Location: ".$url); die; return;
			}
		}
		
		public static function renderingHTML($isHTML) {
			self::$renderingHTML = $isHTML;
		}
		
		public static function renderingPlainText($isPlainText) {
			self::$renderingPlainText = $isPlainText;
		}
		
		public static function ShouldRenderAsHTML() {
			if (self::$renderingHTML) {
				return true;
			} else if (self::$renderingPlainText) {
				return false;				
			} else if (self::$shouldRenderAsHTML == null) {
				$path = $_SERVER['SCRIPT_NAME'];
				self::$shouldRenderAsHTML = (stripos($path, '/ajax/') === false) && (stripos($path, '/interface/') === false);
			}
			return self::$shouldRenderAsHTML;
		}

		public static function SetPageTitle($title) {
			self::$pageTitle = $title;
		}

		public static function SetBodyClass($class) {
			self::$bodyClass = $class;
			ZCode::addToZCodeForJS('pageType', $class);
		}

		public static function GetPageTitle($tag = null, $class = null) {
			if (empty(self::$pageTitle)) {
				return '';
			}
			if (empty($tag)) {
				return self::$pageTitle;
			}
			return '<' . $tag . ' class="' . $class . '">' . self::$pageTitle . '</' . $tag . '>';
		}

		public static function Redirect($aPage, $aMessage="") {
			http_response_code(HTTP_SEE_OTHER);
			header("Location: " . $aPage);
			die($aMessage. '<br>'."Location: " . $aPage);
		}

		public static function Die($msg = "Illegal Operation") {
			self::ShouldRenderAsHTML() && self::Header();
			echo '<div class="warning">'.$msg.'</div>';
			self::ShouldRenderAsHTML() && self::Footer();
			die;
		}

		public static function ExitPage($message, $_pageName, $statusCode = 200) {
			global $pageName;
			$html = self::ShouldRenderAsHTML();
			$pageName = $_pageName;
			http_response_code($statusCode);
			self::ShouldRenderAsHTML() && self::Header();
			echo $message;
			if (self::ShouldRenderAsHTML()) {
				self::Footer();
			} else if ($statusCode != 200) { 
				ZLog::Print(); 
			}
			die;
		}

		public static function Error($message, $errors = array()) {
			if (function_exists('ZAuth_HandleError')) {
				ZAuth_HandleError($message, $errors);	
			}
			if (!empty($errors)) {
				$message .= '<pre>' . print_r($errors, true) . '</pre>';
			}
			$message .= '<pre>' . print_r(ZUtils::GetStackTrace(), true) . '</pre>';
			ZRequest::ExitPage($message, 'Error', HTTP_BAD_REQUEST);
		}
		
		public static function Forbidden($message) { ZRequest::ExitPage($message, 'Forbidden', 403); }
		public static function Notice($message) { ZRequest::ExitPage($message, 'Notice'); }

		public static function SetFlash($msg) {
			$_SESSION['flash_msg'] = $msg;
		}

		public static function GetFlash($class = false) {
			if (empty($_SESSION['flash_msg'])) {
				return false;
			}
			$msg = $_SESSION['flash_msg'];
			unset($_SESSION['flash_msg']);
			if ($class) {
				return '<div class="'.$class.'">'.$msg.'</div>';
			}
			return $msg;
		}

		public static function AddBreadcrumb($name, $url) {
			static::$breadcrumbs[] = ['name' => $name, 'url' => $url];
		}

		public static function PrintBreadcrumbs() {
			if (!empty(static::$breadcrumbs)) {
				// pr(static::$breadcrumbs);
				$out = '<div class="breadcrumbs">';
				$func = function($bc) {  return '<a class="breadcrumb" href="' . $bc['url'] . '">' . $bc['name'] . '</a>'; };

				$out .= implode(' &gt; ', array_map($func, static::$breadcrumbs));
				// foreach (static::$breadcrumbs as $bc) {

				// }
				$out .= '</div>';
				return $out;
			}
			return '';
		}

		public static function SetSessionVariable($key, $msg) {
			$_SESSION[$key] = $msg;
		}

		public static function GetSessionVariable($key, $unset = true) {
			if (empty($_SESSION[$key])) {
				return false;
			}
			$msg = $_SESSION[$key];
			if ($unset) {
				unset($_SESSION[$key]);
			}
			return $msg;
		}

		public static function DieIfError() {
			if (!empty(ZLog::$errors)) {
				ZRequest::Error('There were Request Errors', ZLog::$errors);
			}
		}

		public static function DieIfErrors() {
			ZRequest::DieIfError();
		}

		public static function OutputAsDownload($fileName, $content) {
			$size = mb_strlen($content);
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . $fileName); 
			header('Content-Transfer-Encoding: binary');
			header('Connection: Keep-Alive');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . $size);
			echo $content;
			die;
		}

		public static function OutputIfTesting($id) {
			if (ZAuth::$automatedTests) {
				echo $id;
				die;
			}
		}

		public static function Push($msg) {
			echo $msg;
			ob_flush();
			flush();
		}


	}
	
	
	
	
	