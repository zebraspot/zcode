<?php
	class ZGD {
		public static function OpenImage ($file) {
			global $debug;
			list($width, $height, $type, $attr) = getimagesize($file);
			$metadata = @exif_read_data($file);
			
			$fileType = image_type_to_mime_type($type);
			switch($fileType) {
				case 'image/jpeg':
					$im = @imagecreatefromjpeg($file);
					break;
				case 'image/pjpeg':
					$im = imagecreatefromjpeg($file);
					break;
				case 'image/gif':
					$im = @imagecreatefromgif($file);
					break;
				case 'image/png':
					$im = @imagecreatefrompng($file);
					break;
				default:
					$im = false;
				break;
			}
			if ($metadata['Orientation'] == 3) {
				$im = imagerotate($im, 180, 0);
			} else if ($metadata['Orientation'] == 6) {
				$im = imagerotate($im, 270, 0);
			} else if ($metadata['Orientation'] == 8) {
				$im = imagerotate($im, 90, 0);
			}
			return $im;
		}
	

		public static function getImageInfo($file) {
			list($width, $height, $type, $attr) = getimagesize($file);
			$metadata = @exif_read_data($file);
			foreach ($metadata as $key => $data) {
				// if (is_array($data) && count($data) > 16) {
				if ($key == 'ModeArray' || $key == 'CustomFunctions' || $key == 'ImageInfo' || stripos($key, 'UndefinedTag') !== false) {
					unset($metadata[$key]);
				}
			}
			return ['width' => $width, 'height' => $height, 'type' => image_type_to_mime_type($type), 'attr' => $attr, 'metadata' => $metadata];
		}

		public static function MakeThumb($srcFile, $targetFile, $targetW=60, $targetH=80, $border=0) {
	//		echo "MakeThumb(): Source File: $srcFile , Destination: $targetFile <br>";
			$image = ZGD::OpenImage($srcFile);
			if ($image === false) { ZRequest::Die('Unable to open image'); }
			
			$targetRatio = $targetW/$targetH;
			
			$srcW = imagesx($image);
			$srcH = imagesy($image);
			$srcRatio = $srcW / $srcH;
	
			if ($srcRatio < $targetRatio) {  // too tall
				$scrX = 0;
				$srcH = $srcW / $targetRatio;
				$srcY = (imagesy($image) - $srcH) / 2;
			} else {   // too wide
				$srcY = 0;
				$srcW = $srcH * $targetRatio;
				$srcX = (imagesx($image) - $srcW) / 2;
			}
	
	
			// Resample
			$image_resized = imagecreatetruecolor($targetW, $targetH);
			$targetW = $targetW - (2 * $border);
			$targetH = $targetH - (2 * $border);
			imagefilledrectangle ($image_resized , $border , $border , $targetW , $targetH , 0xFFFFFF );
			imagecopyresampled($image_resized, $image, $border, $border, $srcX, $srcY, $targetW, $targetH, $srcW, $srcH);
			
			if ($targetFile != NULL) {				// sove resized image
				imagejpeg($image_resized, $targetFile);
			} else {										// return resized image
				ob_start();
				imagejpeg($image_resized);
				$imageData = ob_get_contents();
				ob_end_clean();
				return $imageData;
			}
	
		}
	
	
		public static function ScaleImage($srcFile, $targetFile, $targetW, $targetH, $scaleMode='max') {
			global $debug;
	/* 		if ($debug) echo "scaleImage: Source File: $srcFile , Destination: $targetFile <br>"; */
			$image = ZGD::OpenImage($srcFile);
			if ($image === false) { ZRequest::Die('Unable to open image'); }
			
			$targetRatio = $targetW/$targetH;
			$srcW = imagesx($image);
			$srcH = imagesy($image);
	
			if ($scaleMode == 'max') {
				$srcRatio = $srcW / $srcH;
				if ($srcRatio < $targetRatio) {  // too tall
					$targetW = $targetH * $srcRatio;
				} else {   // too wide
					$targetH = $targetW / $srcRatio;
				}
			} else if ($scaleMode == 'none') {
				
			}
				
			// Resample
			$image_resized = imagecreatetruecolor($targetW, $targetH);
			imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $targetW, $targetH, $srcW, $srcH);
			$finalW = floor($targetW);
			$finalH = floor($targetH);
			// Display resized image
	/* 		pr ('Target file: '. $targetFile);		die ($targetFile); */
			if ($targetFile != NULL) {
				@unlink($targetFile);
				$result = imagejpeg($image_resized, $targetFile);
	/* 			if ($debug) echo "Image write: $targetFile , Result: ".($result?'success':'failure')." <br>"; */
				if ($result == false) {
					ZRequest::Die('Could not write to file '.$targetFile);
				}
			} else {
				ob_start();
				imagejpeg($image_resized);
				$imageData = ob_get_contents();
				ob_end_clean();
				return $imageData;
			}
		}
		
/*
		function scaleImage($srcFile, $targetFile, $targetW, $targetH, $scaleMode='max') {
			global $finalW, $finalH, $debug;
			$image = open_image($srcFile);
			if ($image === false) { VTBdie ('Unable to open image'); }
			
			$targetRatio = $targetW/$targetH;
			$srcW = imagesx($image);
			$srcH = imagesy($image);
	
			if ($scaleMode == 'max') {
				$srcRatio = $srcW / $srcH;
				if ($srcRatio < $targetRatio) {  // too tall
					$targetW = $targetH * $srcRatio;
				} else {   // too wide
					$targetH = $targetW / $srcRatio;
				}
			} else if ($scaleMode == 'keep') {
				$srcRatio = $srcW / $srcH;
				if ($srcRatio < $targetRatio) {  // too tall
					$scale = $targetH / $srcH;
				} else {   // too wide
					$scale = $targetW / $srcW;
				}
				$targetH = $srcH * $scale;
				$targetW = $srcW * $scale;
			} else if ($scaleMode == 'none') {
				
			}
				
			// Resample
			$image_resized = imagecreatetruecolor($targetW, $targetH);
			imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $targetW, $targetH, $srcW, $srcH);
			$finalW = floor($targetW);
			$finalH = floor($targetH);
			// Display resized image
			if ($targetFile != NULL) {
				@unlink($targetFile);
				$result = imagejpeg($image_resized, $targetFile);
				if ($result == false) {
					VTBdie('Could not write to file '.$targetFile);
				}
			} else {
				ob_start();
				imagejpeg($image_resized);
				$imageData = ob_get_contents();
				ob_end_clean();
				return $imageData;
			}
		}
*/
	
	
	
	
		public static function SaveImage($FILE, $thumbFile, $origFile = null) {
			global $SitePath;
			if (!empty($FILE) && $FILE['error'] == 0) {
				if ($origFile != null) {
					ZGD::ScaleImage($FILE['tmp_name'], $origFile, 1024,1024);
				}
				if ($thumbFile != null) {
					ZGD::MakeThumb($FILE['tmp_name'], $thumbFile,400,533);
				}
			} else if ($FILE['error'] != 4) {
				$error = 'There was an error uploading the mugshot file.';
				ZLog::error('Image Process Error!', 'There was an error uploading the mugshot file.', 'parse');
				return $error;
			}
		}
	
	}
	
	