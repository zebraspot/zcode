<?php
	// -----------------------------------------------------------------------------------------------------------
	// -----------------------------------   Database interaction code   -----------------------------------------
	// -----------------------------------------------------------------------------------------------------------
	
	class ZDB {
		public const DELETE_ONE = ' LIMIT 1';
		public const DELETE_ALL = '';
		private static $error = false;
		private static $errors = array();
		private static $links = array();

		private static $dbs;
		private static $currentDB = 'primary';
		private static $groupedInserts;
		private static $groupedUpdates;
		private static $tableDescriptions;
		private static $printNextQuery = false;
		
		public static function Init() {
			global $ZCodeConf;
			self::$dbs = $ZCodeConf['dbs'];
		}
		
		public static function SetDB($dbName) {
			self::$currentDB = $dbName;
		}

		public static function GetDBName() {
			return self::$dbs[self::$currentDB]['db'];
		}

		public static function ResetDB() {
			self::$currentDB = 'primary';
		}

		public static function Connect() {
			if (!empty(self::$links[self::$currentDB])) {
				return self::$links[self::$currentDB];
			}
			$db = self::$dbs[self::$currentDB];
			self::$links[self::$currentDB] = new PDO('mysql:host='.$db['host'].';dbname='.$db['db'], $db['user'], $db['password']);	
		 	self::$links[self::$currentDB]->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			self::$links[self::$currentDB]->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			self::$links[self::$currentDB]->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
			return self::$links[self::$currentDB];
		}
	
		public static function Disconnect() {
			self::$links = array();
		}
	
		public static function Exec($sql) {
			try {
				$grLink = self::Connect();
				$result = $grLink->exec($sql);
			} catch( PDOException $Exception ) {
				return $Exception->getMessage( );
			}
			return true;
		}

		public static function GetQuery($query, $params = array(), $options = array()) {
			global $verbose;
			self::$error = false;
			
			$list = array();
			if (isset($query)) {
				$grLink = self::Connect();
				ZLog::log('GetQuery: ' . $query, $params,  'SQL', true);
				ZLog::Increment('SQLQueries');
				self::pr( $params, $query);
				try {
					$statement = $grLink->prepare($query);
					$success = $statement->execute($params);
				} catch( PDOException $Exception ) {
					self::$errors[] = self::$error = $Exception->getMessage( );
					ZLog::Error('PDO Query Error!', self::$error, 'SQL', true);
					self::pr(self::$error);
					return false;
				}
	
				try {
					if ($success) {
						while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
							if (isset($row['index'])) {
								$list[$row['index']] = $row;
							} else {
								$list[] = $row;
							}
						}
					}
				} catch( PDOException $Exception ) {
					$error = $Exception->getMessage();
					if ($error && strpos($error, 'General error') === false) {
						self::$error = $error;
						ZLog::Error('PDO Result Error!', self::$error, 'SQL', true);
						self::$errors[] = 'SQL Error: ' . $error;
					}
					return true;
				}
			}
			ZLog::log('QueryResult', $list, 'SQL');
			if (!empty($options['keyedBy'])) {
				$list = self::SortRecords($list, $options['keyedBy'], @$options['keyPrepend']);
			}
			self::$printNextQuery = false;
			return $list;
		}

		public static function pr($out, $label=false) {
			global $verbose;
			if ($verbose || self::$printNextQuery) {
				return ZUtils::pr($out, $label);
			}
		}

		public static function GetRecord($table, $field, $value=0, $append='') {
			$results = self::GetRecords($table, $field, $value, $append . ' LIMIT 0,1');
			self::pr($results, 'GetRecord unfiltered Result');
			if (!empty($results)) {
				return $results[0];
			}
			return array();
		}
		
		public static function GetRecords($table, $field, $value = 0, $append = '') {
			if (is_array($field)) {
				$query = "SELECT * FROM $table WHERE 1 ";
				$params = array();
				foreach ($field as $key => $value) {
					$query .= ' AND `'.$key.'` = ?';
					$params[] = $value;
				}
				$query .= $append;
				$result = self::GetQuery($query, $params);
				return $result;
			}
			$query = "SELECT * FROM $table WHERE `status`> -1 AND `$field` LIKE ? " . $append;
			$result = self::GetQuery($query, array($value));
			return $result;
		}
	
		public static function InsertOrUpdate($table, $fields) {
			if (!empty($fields['id'])) {
				return self::Update($table, $fields);
			} else {
				unset($fields['id']);
				return self::Insert($table, $fields);
			}
		}
	
		public static function Insert($table, $fields) {
			$sql = 'INSERT INTO ' . $table . ' (`'.implode('`, `', array_keys($fields)).'`) VALUES ('.self::QuestionMarks(count($fields)).');';
			$params = array_values($fields);
			self::GetQuery($sql, $params);
			self::DieIfError();
			$records = self::GetQuery('SELECT * FROM ' . $table . ' WHERE id = ? LIMIT 1;', array(self::GetLastInsertId()));
			return $records[0];
		}
	
		public static function InsertGrouped($table, $fields = null)
		{
			if (empty(self::$groupedInserts[$table])) {
				if (!empty($fields)) {
					self::$groupedInserts[$table]['columns'] = array_keys($fields);
					self::$groupedInserts[$table]['rows'] = [array_values($fields)];
				}
			} else {
				if ($fields) {
					$vals = [];
					foreach (self::$groupedInserts[$table]['columns'] as $colName) {
						$vals[] = $fields[$colName];
					}
					self::$groupedInserts[$table]['rows'][] = $vals;
					// echo json_encode($vals) . '<br />';
				}
				$rowCount = count(self::$groupedInserts[$table]['rows']);
				if (!$fields || $rowCount >= 500) {
					$columCount = count(self::$groupedInserts[$table]['columns']);
					$sql = 'INSERT IGNORE INTO ' . $table . ' (`' . implode('`, `', self::$groupedInserts[$table]['columns']) . '`) VALUES';
					$params = [];
					$valueSets = [];
					foreach (self::$groupedInserts[$table]['rows'] as $fields) {
						$params = array_merge($params, $fields);
						$valueSets[] = '(' . ZDB::QuestionMarks($columCount) . ')';
					}
					$sql .= implode(', ', $valueSets);
					ZDB::GetQuery($sql, $params);
					unset(self::$groupedInserts[$table]);
				}
			}
		}



		public static function Update($table, $fields) {
			$id = $fields['id'];
			$fieldSets = array();
			$params = array();
			foreach ($fields as $fieldName => $fieldValue) {
				if ($fieldName != 'id') {
					$fieldSets[] = '`'.$fieldName.'` = ?';
					$params[] = $fieldValue;
				}
			}
			$sql = 'UPDATE '.$table.' SET '.implode(', ', $fieldSets) . ' WHERE id = ?;';
			$params[] = $fields['id'];
			self::GetQuery($sql, $params);
			return self::GetRecord($table, 'id', $id);
		}

		private static function GetUpdateSql($table, $fieldNames) {
			$fieldSets = array();
			foreach ($fieldNames as $fieldName) {
				if ($fieldName != 'id') {
					$fieldSets[] = '`'.$fieldName.'` = ?';
				}
			}
			return 'UPDATE '.$table.' SET '.implode(', ', $fieldSets) . ' WHERE id = ?;';
		}
	
		private static function RunUpdateQueries($query, $paramSets) {
			global $verbose;
			self::$error = false;
			
			if (isset($query)) {
				$grLink = self::Connect();
				ZLog::log('RunUpdateQueries: ' . $query,  'SQL', true);
				self::pr( $paramSets, $query);
				try {
					$statement = $grLink->prepare($query);
					foreach ($paramSets as $id => $params) {
						$statement->execute(array_merge($params, [$id]));
					}
				} catch( PDOException $Exception ) {
					self::$errors[] = self::$error = $Exception->getMessage( );
					ZLog::Error('PDO RunUpdateQueries Error!', self::$error, 'SQL', true);
					self::pr(self::$error);
					return false;
				}
			}
		}

		public static function UpdateGrouped($table, $fields = null) {
			if (empty(self::$groupedUpdates[$table])) {
				if (!empty($fields)) {
					if (empty($id = $fields['id'])) {
						throw new Exception('UpdateGrouped requires fields to contain `id`.');
					}
					unset($fields['id']);
					self::$groupedUpdates[$table]['columns'] = array_keys($fields);
					self::$groupedUpdates[$table]['rows'] = [$id => array_values($fields)];
				}
			} else {
				if ($fields) {
					if (empty($id = $fields['id'])) {
						throw new Exception('UpdateGrouped requires fields to contain `id`.');
					}
					$vals = [];
					foreach (self::$groupedUpdates[$table]['columns'] as $colName) {
						$vals[] = $fields[$colName];
					}
					self::$groupedUpdates[$table]['rows'][$id] = $vals;
					// echo json_encode($vals) . '<br />';
				}

				$rowCount = count(self::$groupedUpdates[$table]['rows']);
				if (!$fields || $rowCount >= 500) {
					$sql = ZDB::GetUpdateSql($table, self::$groupedUpdates[$table]['columns']);
					ZDB::RunUpdateQueries($sql, self::$groupedUpdates[$table]['rows']);
					unset(self::$groupedUpdates[$table]);
				}
			}
		}

		public static function CountRecords($table, $field, $value) {
			if (isset($value)) {
			    $row = self::GetQuery(
			    	"SELECT count(*) as count FROM $table WHERE `status`> -1 AND $field LIKE ? ",
			    	array($value)
			    );
			}
			return $row[0]['count'];
		}
		
		public static function GetLastInsertId() {
			$grLink = self::Connect();
			$id = $grLink->lastInsertId();
			return $id;
		}
		
		public static function Delete($table, $field, $value, $mode = ZDB::DELETE_ONE) {
			self::GetQuery(
				"DELETE FROM $table WHERE `$field` = ? $mode",
				array($value)
			);
		}
		
		public static function SortRecords($list, $keyId, $prepend=null) {
			$out = array();
			foreach ($list as $rec) {
				$key = $prepend == null ? $rec[$keyId] : strval($prepend . $rec[$keyId]);
				$out[$key] = $rec;
			}
			return $out;
		}
			
		public static function DiffToString($record1, $record2) {
			$out = '';
			foreach ($record1 as $key => $value) {
				if ($record1[$key] != $record2[$key]) {
					$value2 = $record2[$key];
					$out .= "$key changed from '$value' to '$value2', ";
				}
			}
			if (empty($out)) {
				return false;
			}
			return substr($out, 0, -2);
		}
		
		public static function RecordHash($record, $fields) {
			return md5('fsdgdgsdgertwew45674ufgd' . json_encode($record));
		}
			
		public static function Safe($value, $filter=null) {
			if ($filter == 'int')
				return intval($value);
			else if ($filter == 'float')
				return floatval($value);
			else if ($filter == 'email')
				return preg_replace('/[^a-zA-Z0-9*._-+]/', '', $value);
			else if ($filter == 'alphanum')
				return preg_replace('/[^a-zA-Z0-9_]/', '', $value);
			else if ($filter == 'alphanumplus')
				return preg_replace('/[^a-zA-Z0-9_ ]/', '', $value);
			else 
				return addslashes($value);
		}
	
		public static function QuestionMarks($num) {
			if (is_array($num)) {
				$num = count($num);
			}
			$questionmarks = '';
			for ($i=0; $i<$num; $i++) {
			    $questionmarks .= "?,";
			}
			$questionmarks = trim($questionmarks, ",");
			return $questionmarks;
		}
		
		public static function DieIfError() {
			if (self::HasError()) {
				ZRequest::Error('There were SQL Errors',self::$errors);
			}
		}

		public static function HasError() {
			return !empty(self::$errors);
		}

		public static function BuildTableDescriptions($asJson = false) {
			$tablesData = ZDB::GetQuery('SHOW tables');
			$tablesJson = [];
			foreach ($tablesData as $tableData) {
				$tableName = array_shift($tableData);
				$tablesJson[$tableName] = [];

				$columnsData = ZDB::GetQuery('DESCRIBE '.$tableName);
				foreach ($columnsData as $columnData) {
					@list($type, $len, $last) = preg_split('/[\(\) ]+/', $columnData['Type']);
					$colJson = [];
					$colJson['type'] = $type;
					if (!empty($len)) $colJson['len'] = $len;
					if ($last == 'unsigned') $colJson['unsigned'] = 1;
					if (!empty($columnData['Key'])) $colJson['key'] = $columnData['Key'];
					if ($columnData['Default'] != NULL) $colJson['default'] = $columnData['Default'];
					if ($columnData['Extra'] != NULL) $colJson['extra'] = $columnData['Extra'];
					$colJson['allowNull'] = ($columnData['Null'] == 'YES' ? 1 : 0);
					$tablesJson[$tableName][$columnData['Field']] = $colJson;
				}
			}
			if ($asJson) {
				return json_encode($tablesJson, JSON_PRETTY_PRINT);
			}
			return $tablesJson;
		}

		public static function getTableDescriptionData($tableName = NULL, $fieldName = NULL, $fieldAttr = NULL) {
			static::loadTableDescriptionsFromConfig();
			$out = static::$tableDescriptions;
			if (!empty($tableName)) {
				$out = $out[$tableName];
				if (empty($out)) {
					throw new Exception("Table '$tableName' not exist in schema descriptor json", 1);
				}
				if (!empty($fieldName)) {
					$out = $out[$fieldName];
					if (empty($out)) {
						throw new Exception("Field '$tableName.$fieldName' not exist in schema descriptor json", 1);
					}
					if (!empty($fieldAttr)) {
						$out = $out[$fieldAttr];
					}
				}
			}
			return $out;
		}

		public static function loadTableDescriptionsFromConfig() {
			if (empty(static::$tableDescriptions)) {
				$base = json_decode(file_get_contents(ZCode::$path . '../config/schema/base.json'), true);
				$custom = json_decode(file_get_contents(ZCode::$path . '../config/schema/custom.json'), true);
				static::$tableDescriptions = array_replace_recursive($base, $custom);
			}
		}

		public static function prNextQuery() {
			self::$printNextQuery = true;
		}
	}
	