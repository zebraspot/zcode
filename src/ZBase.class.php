<?php
	class ZBase {

		public static function Config($paramKeys) {
			global $ZCodeConf;

			if (is_string($paramKeys)) {
				$paramKeys = explode('/', $paramKeys);
			}

			$params = $ZCodeConf;
			foreach ($paramKeys as $paramKey) {
				$params = @$params[$paramKey];
			}
			
			if (!empty($params)) {
				foreach ($params as $name => $value) {
					static::$$name = $value;
				}
			}
		}
		
	}