<?php
	// -----------------------------------------------------------------------------------------------------------
	// ------------------------------   Amazon Simple Storage Service code   -------------------------------------
	// -----------------------------------------------------------------------------------------------------------
	// Setup:
	// 	 create /.aws/config and /.aws/credentials as per https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html
	//   Add to site.conf.php:
	//			 'aws' => array(
	//			 	's3' => array(
	//			 		'profile' => 'impydimps',
	//			 		'defaultBucket' => 'impydimps-dev',
	//			 		'region' => 'us-east-1'
	//			 	)
	//			 )
	// Usage:
	//   $result = ZAwsS3::get('304_DSCN2237.JPG', 'impydimps-dev');

class ZAwsS3 extends ZBase {
	private static $client;
	public static $profile;
	public static $version = 'latest';
	public static $region;
	public static $defaultBucket;

		public static function Init() {
			self::Config('aws/s3');
		}

		private static function GetClient() {
			if (!(static::$client)) {
				static::$client = new Aws\S3\S3Client([
					'profile' => static::$profile,
					'version' => static::$version,
					'region'  => static::$region
				]);
			}
			return static::$client;
		}

		
	public static function put($filePath, $storageName, $bucket = null) {
		$client = static::GetClient();
		$bucket = (empty($bucket)) ? static::$defaultBucket : $bucket;
		// pr($storageName, 'storageName');
		try {
			$client->putObject([
				'Bucket' => $bucket,
				'Key'    => $storageName,
				'Body'   => fopen($filePath, 'r'),
				// 'ACL'    => 'public-read',
			]);
		} catch (Aws\S3\Exception\S3Exception $e) {
			ZLog::Error($e->__toString());
			return false;
		}
		return true;
	}

	public static function get($storageName, $bucket = null) {
		$client = static::GetClient();
		$bucket = (empty($bucket)) ? static::$defaultBucket : $bucket;
		try {
			$result = $client->getObject([
				'Bucket' => $bucket,
				'Key'    => $storageName,
			]);
		} catch (Aws\S3\Exception\S3Exception $e) {
			ZRequest::Error($e->__toString(), 'S3 Error');
			return false;
		}
		return $result;
	}

	public static function save($fileName, $storageName, $bucket = null) {
		$result = static::get($storageName, $bucket);
		file_put_contents($fileName, $result['Body']);
	}

	public static function delete($storageName, $bucket = null) {
		$client = static::GetClient();
		$bucket = (empty($bucket)) ? static::$defaultBucket : $bucket;
		try {
			$client->deleteObject([
				'Bucket' => $bucket,
				'Key'    => $storageName,
			]);
		} catch (Aws\S3\Exception\S3Exception $e) {
			ZRequest::Error($e->__toString(), 'S3 Error');
			return false;
		}
	}

}