<?php
	class ZMigrates extends ZBase {
		public static $completedMigrates;

		public static function Init() {
			self::Config('migrates');
		}

		public static function getCompletedMigrates() {
			if (!static::$completedMigrates) {
				static::$completedMigrates = ZDB::GetQuery('SELECT `name` from `migrates`;', [], ['keyedBy' => 'name']);
			}
			return static::$completedMigrates;
		}

		public static function hasRun($name) {
			$cm = static::getCompletedMigrates();
			return isset($cm[$name]);
		}

		public static function getMigratesToRun() {
			$migrates = scandir(ZCode::$path . '../config/schema/migrates/');
			$migrates = array_filter($migrates, function($m) { return substr($m, -4) == '.sql';});
			$migrates = array_map(function($m) { return substr($m, 0, -4);}, $migrates);
			$migrates = array_filter($migrates, function($m) { return !static::hasRun($m);});
			return $migrates;
		}

		public static function runMigrates() {
			$migrates = static::getMigratesToRun();
			foreach ($migrates as $migrate) {
				$path = ZCode::$path . '../config/schema/migrates/'.$migrate.'.sql';
				$sql = file_get_contents($path);
				$result = ZDB::Exec($sql);
				pr($result, 'Exec(' . $path . ') result.');
				ZDB::Insert('migrates', ['name' => $migrate, 'status' => 0, 'created' => time()]);
			}
		}

	}
	
	
	
	
	
