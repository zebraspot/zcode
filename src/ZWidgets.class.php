<?php

	class ZWidgets extends ZBase {

		public static function QuickTable($rows, $options = array()) {
			$firstRow = array_values($rows)[0];
			if (is_object($firstRow)) {
				$keys = $firstRow->getTableHeaders();
			} else {
				$keys = array_keys($firstRow);
			}
			$s = '<table><tr>';
			foreach ($keys as $key) {
				$s .= '<th>'.$key.'</th>';
			}
			$s .= '</tr>';
			foreach ($rows as $row) {
				$s .= '<tr>';
				if (is_object($row)) {
					$row = $row->getTableRow();
				} 
				foreach ($keys as $key) {
					$value = $html = $row[$key];
					if ($options[$key]) {
						if (!empty($options[$key]['joinRecord'])) {
							$html = $options[$key]['joinRecord'][$value]['name'];
						}
						if ($options[$key]['linkFragment']) {
							$html = '<a href="'.$options[$key]['linkFragment'].$value.'">'.$html.'</a>';
						}
						if ($options[$key]['type'] == 'date') {
							$html = empty($value) ? '' : date(DATE_FORMAT, $value);
						}
					}
					$s .= '<td>'.$html.'</td>';
				}
				$s .= '</tr>';
			}
			$s .= '</table>';
			return $s;
		}

		public static function Expand($content, $link = 'Click to expand...') {
			$id = 'd' . rand(1000,10000000);
			$s = '<div class="expand">';
			$s .= '<a href="#" onclick="$(\'#'.$id.'\').toggle(); return false;">'.$link.'</a>';
			$s .= '<div id="'.$id.'" style="display:none;">'.$content.'</div>';
			$s .= '</div>';
			return $s;
		}

	}
