<?php
	class ZPermissionException extends Exception {}
	class ZRecordChangedException extends Exception {}

	class ZTable {
		public static $timestamped = true;
		public static $setCreator = false;
		public static $formOptions = [];
		public static $viewLink = '/record/view.php?id=';
		public static $enablePublic = false;
		public static $restrictable = false;
		public static $manyToManyJoins = [];
		public static $baseOptionsForSelect = ['' => 'None'];
		public static $recordName = null;
		public static $displayNameColumn;
		public static $preventSaveIfChanged = true;
		public $public = false;

		// public static $recordValues = [];

		public $data;
		public $id;
		public $metadata;
		public $fromDB;

		public static function Init() {
			// Failure here indicates that you need to declare $recordValues in the childclass. ie:
			// public static $recordValues = [];
			static::$recordValues['initialized'] = true;
		}

		/**
		 * Returns a single object with the id specified. 
		 * Retruns Null if the id is empty.  
		 * Throws an Error if the id is not found.
		 */ 
		public static function Get($id) {
			if (empty($id)) { return null; }
			if (is_array($id)) {
				$record = static::GetSingleRecord($id);
				if (empty($record)) {
					throw new Exception('ZTable->Get( '.json_encode($id).' ) not found in '.static::$tableName);
				}
				return $record;
			}
			$data = ZDB::GetQuery('SELECT * FROM ' . static::$tableName . ' WHERE status >= 0 AND id = ? LIMIT 1', [$id]);
			if (empty($data)) {
				throw new Exception('ZTable->Get( id = '.$id.' ) not found in '.static::$tableName);
			}
			$out = new static($data[0], $fromDB = true);
			return $out;
		}

		public static function GetPublic($id, $hash) {
			if (!static::$enablePublic || empty($id) || static::GetLinkHash($id) != $hash) { return null; }
			$r = static::$restrictable;
			static::$restrictable = false;
			$out = static::Get($id);
			static::$restrictable = $r;
			if (!$out->data['public']) {
				return null;
			}
			$out->public = true;
			return $out;
		}

		/**
		 * Returns the object with the id specified, or an empty object, if the id is empty.
		 * if id is non-empty, but the record cannot be found, throws an error.
		 */
		public static function GetOrNew($id) {
			if (empty($id)) { return new static([]); }
			$data = ZDB::GetQuery('SELECT * FROM ' . static::$tableName . ' WHERE status >= 0 AND id = ? LIMIT 1', [$id]);
			if (empty($data)) {
				throw new Exception('ZTable->Get( id = '.$id.' ) not found in '.static::$tableName);
			}
			return new static($data[0], $fromDB = true);
		}

		/**
		 * Saves data from an associative array, where keys represent the field names
		 * if 'id' is present, saves over the existing record with the same id.
		 * If 'id is not present, creates a new record.
		 */
		public static function SaveFromPost($data) {
			try {
				$rec = new static($data);
			} catch (ZPermissionException $e) {
				ZRequest::Error('You cannot update a '. static::getRecordName() . ' ownership or restrictions in such a way that would prevent you from being able to access it. The record was not saved.');
			}

			$rec->save();
			return $rec;
		}

		/**
		 * Returns an array of objects based on the options passed in.
		 * See https://bitbucket.org/zebraspot/zcode/src/master/docs/ZTable.md for examples.
		 */
		public static function GetAll($options = []) {
			$datas = static::GetQuery($options);
			return static::ObjectsFromQueryResult($datas, $options);
		}

		/**
		 * Similar to GetAll(), but just returns a flat list of ids that match the criteria.
		 */
		public static function GetAllIds($options = []) {
			$col = static::$tableName . '.id as id';
			$datas = static::GetQuery($options, $col);
			$toId = function($n) {
				return intval($n['id']);
			};
			return array_map($toId, $datas);
		}

		/**
		 * Similar to GetAll(), but just returns a count of rows that match the criteria.
		 */
		public static function CountAll($options = []) {
			if (empty($options['selectSpecifier'])) {
				$options['selectSpecifier'] = 'count(*) as count';
			}
			$countResult = static::GetQuery($options);
			return $countResult[0]['count'];
		}

		private static function GetQuery($options = []) {
			$params = [];
			$options = static::addRestrictions($options);
			$whereString = static::GetWhereStringFromOptions(@$options['where'], $params);
			$orderString = static::GetOrderStringFromOptions(@$options['order']);
			$limitString = static::GetLimitStringFromOptions(@$options['limit']);
			$joinString = static::GetJoinStringFromOptions(@$options['join']);
			if (empty($options['selectSpecifier'])) {
				$options['selectSpecifier'] = static::$tableName . '.*';
			}
			$out = ZDB::GetQuery('SELECT '.$options['selectSpecifier'].' FROM ' . static::$tableName . $joinString . ' WHERE '. $whereString . $orderString . $limitString, $params);
			ZDB::DieIfError();
			return $out;
		}

		private static function addRestrictions($options) {
			// todo - check for existing status claus in where
			$options['where'][] = ['column' => 'status', 'table' => static::$tableName, 'comparator' =>  '>=', 'value' => 0];
			if (static::$restrictable) {
				$options['where'][] = ['subwheres' => [
					['column' => 'restrict_to', 'value' => '0'],
					['boolean' => 'OR', 'column' => 'restrict_to', 'comparator' => 'IN', 'value' => ZUser::$loggedIn->GetGroups()],
					['boolean' => 'OR', 'column' => 'user_id', 'value' => ZUser::$loggedIn->id]
				]];
			}
			return $options;
		}

		/**
		 * Similar to GetAll(), but deletes the rows that match the criteria.
		 */
		public static function DeleteAll($where = []) {
			$params = [];
			if ($where['where']) { $where = $where['where']; }
			$whereString = static::GetWhereStringFromOptions($where, $params);
			ZDB::GetQuery('DELETE FROM ' . static::$tableName . ' WHERE '. $whereString, $params);
		}
		

		private static function GetWhereStringFromOptions($whereCriteria, &$params) {
			$wheres = [];
			$defaultCriteria = ['column' => 'id', 'comparator' =>  '=', 'value' => 0, 'boolean' => 'AND'];
			if (!empty($whereCriteria)) {
				$wheres = isset($whereCriteria['column']) ? [$whereCriteria] : $whereCriteria;
			}
			// $wheres[] = ['column' => 'status', 'comparator' =>  '>=', 'value' => 0];
			$toAdd = '';
			$whereString = '';
			foreach ($wheres as $where) {
				$where = array_merge($defaultCriteria, $where);
				if (!empty($whereString)) {
					$toAdd = ' '.$where['boolean'].' ';
				}  
				if (!empty($where['subwheres'])) {
					$toAdd .= ' ( '. static::GetWhereStringFromOptions($where['subwheres'], $params) . ' ) ';
				} else {
					if (!empty($where['table'])) {
						$toAdd .= '`'.$where['table'].'`.';
					}
					$toAdd .= '`'.$where['column'].'` ';
					if ($where['value'] === 'NULL' || $where['value'] === NULL) {
						if ($where['comparator'] == '=') { $where['comparator'] = 'IS'; };
						$toAdd .= $where['comparator'] . ' NULL '; 
					} else {
						$toAdd .= $where['comparator'];
						if (isset($where['column2'])) {
							$toAdd .= ' `'.$where['column2'].'` ';
						} else if (strtolower(trim($where['comparator'])) == 'in' && is_array($where['value'])) {
							if (empty($where['value'])) {
								$toAdd = ' '.$where['boolean'].' FALSE '; 
							} else {
								$toAdd .= ' ('.implode(', ', $where['value']).') ';
							}
						} else {
							$toAdd .= ' ? ';
							$params[] = $where['value'];
						} 
					}
				}
				$whereString .= $toAdd;
			}
			return $whereString;
		}

		private static function GetJoinStringFromOptions($joinCriteria) {
			if (empty($joinCriteria)) { return ''; }
			$defaultCriteria = ['right_table' => '', 'type' => 'LEFT JOIN', 'right_column' => '', 'left_column' => 'id'];
			$joins = isset($joinCriteria['right_table']) ? [$joinCriteria] : $joinCriteria;
			$joinString = '';
			foreach ($joins as $join) {
				if (!empty($joinString)) {
					$joinString .= ' ';
				}
				$rightTableSpecifier = '`'.$join['right_table'].'` ';
				if (!empty($join['alias'])) {
					$rightTableSpecifier = '`'.$join['right_table'].'` as '. $join['alias'];
					$join['right_table'] = $join['alias'];
				}
				$join = array_merge($defaultCriteria, $join);
				$joinString .= $join['type'] . ' '.$rightTableSpecifier.' ON `'.static::$tableName.'`.`'.$join['left_column'].'` = `'.$join['right_table'].'`.`'.$join['right_column'].'` ';
			}
			return ' '.$joinString;
		}

		private static function GetOrderStringFromOptions($orderCriteria) {
			if (empty($orderCriteria)) {
				return '';
			}
			$c = [];

			foreach ($orderCriteria as $i => $order) {
				if (is_array($order)) {
					if (isset($order['function'])) {
						$c[] = $order['function'] . ' ' . (@$order['ascending'] === false ? 'DESC' : 'ASC');
					} else {
						$c[] = '`' . $order['column'] . '` ' . (@$order['ascending'] === false ? 'DESC' : 'ASC');
					}
				} else {
					$c[] = '`'.$order.'`';
				}
			}
			return ' ORDER BY ' . implode(', ', $c);
		}

		private static function GetLimitStringFromOptions($limit) {
			if (empty($limit)) {
				return '';
			}
			return ' LIMIT '. $limit;
		}

		public static function ObjectsFromQueryResult($datas, $options = []) {
			$out = [];
			foreach ($datas as $data) {
				if (isset($options['keyedBy'])) {
					$out[$data[$options['keyedBy']]] = new static($data, $fromDB = true);
				} else {
					$out[] = new static($data, $fromDB = true);
				}
			}
			return $out;
		}

		public static function getRecordName() {
			return static::$recordName ? static::$recordName : static::$tableName;
		}

		public static function saveForm() {
			if (empty($_POST)) return false;
			if (@$_POST['cancel']) return true;
			if (@$_POST['delete']) {
				$obj = static::Get($_POST[static::$tableName]['id']);
				if ($obj->canDelete()) {
					$obj->delete();
				} else {
					throw new Exception('Specified object is not deletable.', 1);
				}
				ZRequest::SetFlash(i18n('ZC-record-deleted', ['recordName' => static::getRecordName(),'displayName' => $obj->getDisplayName()]));
				return true;
			}
			if (@$_POST['save']) {
				$form = new ZForm(static::$tableName, static::$formOptions);
				$obj = new static($form->getPostData());
				$obj->validateFormInput();
				$obj->save();
				ZRequest::SetFlash(i18n('ZC-record-saved', ['recordName' => static::getRecordName(),'displayName' => $obj->getDisplayName()]));
				return $obj;
			}
			return false;
		}

		public function validateFormInput() {
			$colDatas = static::getTableDescription();
			// pr($this->data);
			foreach ($colDatas as $colName => $colData) {
				// pr($colData, $colName);
				if (isset($colData['maxLength']) && strlen($this->data[$colName]) > $colData['maxLength']) {
					throw new Exception("Input field '$colName' was too long.", 1);
				}
				if (isset($colData['stringMask'])) {
					$a = preg_match($colData['stringMask'], $this->data[$colName]);
					// var_dump($a);
					if (!$a) {
						throw new Exception("Input field '$colName' failed StringMask pattern match check.", 1);
					}
				}
			}
			// die;
		}

		public static function search($term) {
			return static::GetAll(['where' => static::getSearchWheres($term)]);
		}

		public static function getTableDescription() {
			if (empty(static::$recordValues['tableDescription'])) {
				static::$recordValues['tableDescription'] = ZDB::getTableDescriptionData(static::$tableName);
			}
			return static::$recordValues['tableDescription'];
		}

		public static function getSearchWheres($term) {
			$tableData = static::getTableDescription();
			$wheres = [];
			foreach ($tableData as $colName => $colData) {
				if ($colData['noSearch']) continue;
				if ($colData['type'] == 'varchar' || $colData['type'] == 'text') {
					$wheres[] = ['column' => $colName, 'value' => "%$term%", 'comparator' => 'LIKE', 'boolean' => 'OR'];
				}
			}
			return $wheres;
		}

		public static function GetPublicSelectOptions() {
			return ['0' => 'Users Only', '1' => 'Anyone with global link' ];
		}



		// Object Code

		function __construct($data = [], $fromDB = false) {
			if (!empty($data['id'])) {
				$this->id = $data['id'];
			} else {
				unset($data['id']);
			}
			if (!empty($data['__metadata'])) {
				$this->metadata = json_decode($data['__metadata'], true);
			}
			unset($data['__metadata']);
			$this->data = $data;
			$this->fromDB = $fromDB;
			$this->CheckPermissions();
		}

		public function __get($name) {
			$tableData = static::getTableDescription();
			if (isset($tableData[$name])) {
				if (isset($this->data[$name])) { 
					return $this->data[$name];
				} 
				return null;
			}

			$trace = debug_backtrace();
			trigger_error(
				'Undefined property via __get(): ' . $name .
				' in ' . $trace[0]['file'] .
				' on line ' . $trace[0]['line'],
				E_USER_NOTICE);
			return null;
		}
		
		public function __isset($name) {
			$tableData = static::getTableDescription();
			if (isset($tableData[$name])) {
				if (isset($this->data[$name])) { 
					return true;
				} 
			}
			return false;
		}
		
		public function getFromDB() {
			if ($this->id) {
				return static::Get($this->id);
			}
			throw new Exception('Attempting to load record that does not exist.');
		}

		public function canEdit() {
			return true;
		}

		public function setStatus($newStatus) {
			$this->data['status'] = $newStatus;
			$this->save();
		}

		function save($updateOnly = false) {
			if (!$this->canEdit()) {
				throw new ZPermissionException("Save Record Permission denied. You are not allowed to modify this record.");
			}
			if ($this->hasChanged()) {
				if (!$this->renderConflictForm()) {
					throw new ZRecordChangedException("Save Record Failed. The record has been changed by another process since the form was loaded.");
				}
			}
			if (static::$timestamped) {
				$time = time();
				if (empty($this->data['id'])) {
					$this->data['created'] = $time;
					if (static::$setCreator && empty($this->data['user_id'])) {
						$this->data['user_id'] = ZAuth::$loggedInUser['id'];
					}
				}
				$this->data['modified'] = $time;
			}
			foreach ($this->data as $key => $value) {
				if ($value === '') {
					$this->data[$key] = NULL;
				}
			}
			if ($updateOnly) {
				$this->data = ZDB::Update(static::$tableName, $this->data);
			} else {
				$this->data = ZDB::InsertOrUpdate(static::$tableName, $this->data);
			}
			ZDB::DieIfError();
			if (!empty($this->data['id'])) {
				$this->id = $this->data['id'];
			}
		}

		function update() {
			$this->save(true);
		}

		function canDelete() {
			return true;
		}

		function delete() {
			$subRecords = $this->GetDependentRecords();
			array_walk($subRecords, function($sr) { $sr->delete(); });
			$this->beforeDelete();
			ZDB::Delete(static::$tableName, 'id', $this->id);
			ZDB::DieIfError();
		}

		function GetDependentRecords() {
			// override to return related zTable records. used by delete();
			return [];
		}

		function beforeDelete() {
			// override to delete table
		}

		public function CheckPermissions() {
			if (static::$restrictable) {
				if (!ZUser::$loggedIn) {
					throw new ZPermissionException("Record Permission denied. You must be logged in to access this record.");
				}

				if (!$this->fromDB && (isset($this->data['id']))) {
					// Record is previously saved, however, we don't know that the user can access the saved data. 
					// Probably a form submission. Let's get the saved record. It'll check permissions on load.
					$rec = static::Get($this->data['id']);
					if (empty($this->data['user_id'])) {
						$this->data['user_id'] = $rec->data['user_id'];
					} else if ($rec->data['user_id'] != $this->data['user_id']) {
						// They are attempting to change record ownership
						if ($rec->data['user_id'] != ZUser::$loggedIn->id) {
							// but they do not own the record!
							throw new ZPermissionException("You must own this record to change its ownership.");
						}
					}
				}
				if ($this->fromDB && empty($this->data['user_id'])) {
					// user_id data is null or zero. Ownership was never set. Let's set it to the current user.
					$this->data['user_id'] = ZAuth::$loggedInUser['id'];
				}
				if (isset($this->data['restrict_to'])) {
					$groupId = $this->data['restrict_to'];
					if ($groupId == 0 || $this->data['user_id'] == ZUser::$loggedIn->id) { return; }
					if ($groupId == -1) {
						throw new ZPermissionException("Record Permission denied. You must own this record to access it.");
					} else if (!in_array($groupId, ZUser::$loggedIn->GetGroups())) {
						throw new ZPermissionException("Record Permission denied. You must belong to a group that has permission to access this record.");
					}
				}
			}
		}

		public function renderConflictForm() {
			return false;
		}

		public function toArray() {
			return $this->data;
		}
                                                                                          
		public function customizeForm($form) {

		}

		public function GetRestrictTag($recordName = null) {
			if (empty($recordName)) {
				$recordName = strtolower(get_class($this));
			}
			return ZForm::GetSelectTag($recordName . '[restrict_to]', ZUserGroup::GetAllForSelect(), $this->data['restrict_to']);
		}

		public function GetMetadataTag($recordName = null) {
			if (empty($recordName)) {
				$recordName = strtolower(get_class($this));
			}
			return ZForm::GetTextTag('hidden', $recordName."[__metadata]", json_encode([
				'fieldhashes' => $this->GetFieldHashes()
			]));
		}

		public function GetPublicTag($recordName = null, $formURL = null) {
			if (!static::$enablePublic) {
				return '';
			}
			if (empty($recordName)) {
				$recordName = strtolower(get_class($this));
			}
			// $out = '<a class="public-link" href="'.$this->GetPublicLink($recordName, $formURL).'">link</a>';
			$out .= ZForm::GetSelectTag($recordName . '[public]', static::GetPublicSelectOptions(), $this->data['public']);
			return $out;
		}

		public function GetOwnerTag($recordName = null) {
			if (empty($recordName)) {
				$recordName = strtolower(get_class($this));
			}
			$userId = empty($this->data['user_id']) ? ZUser::$loggedIn->id : $this->data['user_id'];
			if ($userId != ZUser::$loggedIn->id) {
				$owner = ZUser::Get($userId);
				return $owner->getDisplayName();
			}
			return ZForm::GetSelectTag($recordName . '[user_id]', ZUser::GetAllForSelect(), $userId);
		}

		public function GetPublicLink($recordName, $formURL = '', $wrap = false) {
			$link = '';
			if ($wrap) $link .= '<a class="public-link" href="';
			$link .= $formURL . '?' . $recordName . '=' .$this->id . '&c=' . static::GetLinkHash($this->id);
			if ($wrap) $link .= '">link</a>';
			return $link;
		}

		public static function GetLinkHash($id) {
			return ZUtils::GetHashCode(static::$tableName . 'publicLink' . $id);
		}

		public static function GetFieldsToHash() {
			if (empty(static::$recordValues['fieldsToHash'])) {
				static::$recordValues['fieldsToHash'] = [];
				$tableData = static::getTableDescription();
				foreach ($tableData as $colName => $colData) {
					if ($colData['toHash'] === false) {
						continue;
					}
					if ($colData['toHash'] === true || $colData['type'] == 'text' || ($colData['type'] == 'varchar' && intval($colData['len']) > 250)) {
						static::$recordValues['fieldsToHash'][] = $colName;
					}
				}
			}
			return static::$recordValues['fieldsToHash'];
		}

		public function GetFieldHashes() {
			$fieldsToHash = static::GetFieldsToHash();
			$out = [];
			foreach ($fieldsToHash as $fieldName) {
				$out[$fieldName] = ZUtils::GetHashCode($this->data[$fieldName]);
			}
			return $out;
		}

		public function HasChanged($setOfAvailableFieldsShouldMatch = false) {
			if (!static::$preventSaveIfChanged || !$this->id) {
				return false;
			}
			if (!$this->metadata || !$this->metadata['fieldhashes']) {
				ZLog::Log('Overwrite Save Prevention requested, but no field hashes where found in the data', $this);
				return false;
			}
			$savedRecord = static::Get($this->id);
			$otherHashes = $savedRecord->GetFieldHashes();
			$myHashes = $this->metadata['fieldhashes'];
			if (is_string($myHashes)) {
				$myHashes = json_decode($myHashes, true);
			}
			pr( $myHashes, 'my');
			pr( $otherHashes, 'saved');
			return static::DoFieldHashesNotMatch($myHashes, $otherHashes, $setOfAvailableFieldsShouldMatch);
		}

		public static function DoFieldHashesNotMatch($myHashes, $otherHashes, $setOfAvailableFieldsShouldMatch = false) {
			if ($setOfAvailableFieldsShouldMatch) {
				$keys = array_unique(array_merge(array_keys($myHashes), array_keys($otherHashes)));
			} else {
				$keys = array_intersect(array_keys($myHashes), array_keys($otherHashes));
			}
			foreach ($keys as $key) {
				if ($myHashes[$key] != $otherHashes[$key]) {
					return true;
				}
			}
			return false;
		}

		public function renderForm() {
			$form = new ZForm($this, static::$formOptions);
			$this->customizeForm($form);
			$form->render($this->data);
		}

		public function GetSelectOptions($fieldName) {
			throw new Exception("GetSelectOptions requires subclass override", 1);
		}

		public function getRefLink($colName) {
			if (empty($this->data[$colName])) { return ''; }
			list($className, $id) = explode('_', $colName);
			if ($id == 'id') {
				$className = ucfirst($className);
				if (class_exists($className)) {
					$obj = $className::Get($this->data[$colName]);
					return $obj->getLink(['class' => 'ref-link']);
				}
			}
			
			throw new Exception("GetRefLink requires subclass override", 1);
			// if (empty($this->data['someclass_id'])) { return ''; }
			// if ($colName == 'someclass_id') {
			// 	$someclass = SomeClass::Get($this->data['someclass_id']);
			// 	return $someclass->getLink();
			// }
		}

		public function GetFormHtml($colName) {
			throw new Exception("GetFormHtml requires subclass override", 1);
		}
		
		public static function GetTable($getAllOptions = [], $tableOptions = []) {
			$rows = static::GetAll($getAllOptions);
			return static::GetTableHtml($rows, $tableOptions);
		}

		public static function GetTableHtml($rows, $tableOptions = []) {
			if (empty($rows)) {
				return '<div class="no-results-'.@$tableOptions['class'].'">'.i18n('no-results-'.@$tableOptions['class']).'</div>';
			}
			$keys = static::getTableHeaders();
			$s = '<table class="'.@$tableOptions['class'].'"><tr>';
			foreach ($keys as $key) {
				$s .= '<th>' . $key . '</th>';
			}
			$s .= '</tr>';
			foreach ($rows as $row) {
				$s .= '<tr>';
				$row = $row->getTableRow();
				foreach ($keys as $key) {
					$value = $html = $row[$key];
					if (!empty($tableOptions[$key])) {
						if (!empty($tableOptions[$key]['joinRecord'])) {
							$html = $tableOptions[$key]['joinRecord'][$value]['name'];
						}
						if ($tableOptions[$key]['linkFragment']) {
							$html = '<a href="' . $tableOptions[$key]['linkFragment'] . $value . '">' . $html . '</a>';
						}
						if ($tableOptions[$key]['type'] == 'date') {
							$html = empty($value) ? '' : date(DATE_FORMAT, $value);
						}
					}
					$s .= '<td>' . $html . '</td>';
				}
				$s .= '</tr>';
			}
			$s .= '</table>';
			return $s;
		}

		public static function getTableHeaders()
		{
			return ['Name', 'Summary'];
		}

		public function getTableRow() {
			return ['Name' => $this->getLink(), 'Summary' => $this->getSummary()];
		}

		public function getLink($options = []) {
			$text = !empty($options['text']) ? $options['text'] : $this->getDisplayName();
			$class = !empty($options['class']) ? 'class="'.$options['class'].'"' : '';
			return '<a href="' . $this->getViewLinkURL() . $this->id . '" '.$class.' target="blank">' . $text . '</a>';
		}

		public static function getDisplayNameColumn() {
			if (empty(static::$recordValues['displayNameColumn'])) {
				if (!empty(static::$displayNameColumn)) {
					static::$recordValues['displayNameColumn'] = static::$displayNameColumn;
				} else {
					$tableData = static::getTableDescription();
					foreach ($tableData as $colName => $colData) {
						if (@$colData['type'] == 'varchar') {
							static::$recordValues['displayNameColumn'] = $colName;
							break;
						}
					}
					if (empty(static::$recordValues['displayNameColumn'])) { ZRequest::Error('could not determine table display name'); }
				}
				
			}
			return static::$recordValues['displayNameColumn'];
		}

		public function getDisplayName() {
			$colName = static::getDisplayNameColumn();
			return $this->data[$colName];
		}

		public function getTableName() {
			return static::$tableName;
		}

		public function getSummary() {
			$tableData = static::getTableDescription();
			foreach ($tableData as $colName => $colData) {
				if ($colData['type'] == 'text') {
					return ZUtils::truncate(strip_tags($this->data[$colName]));
				}
			}
			return '';
		}

		public function getViewLinkURL() {
			if (empty(static::$viewLink)) {
				ZRequest::Error('viewLink not set for class ' . get_class($this));
			}
			return static::$viewLink;
		}

		public static function GetAllForSelect($options = []) {
			$colName = static::getDisplayNameColumn();
			if (empty($options['order'])) {
				$options['order'] = ['column' => $colName];
			}
			$key = empty($options['key']) ? 'id' : $options['key'];
			$items = static::GetAll($options);
			$forSelect = empty($options['baseOptions']) ? static::$baseOptionsForSelect : $options['baseOptions'];
			foreach ($items as $item) {
				$forSelect[$item->data[$key]] = $item->getDisplayName();
			}
			return $forSelect;
		}

		public static function GetManyToManyJoinsParams($joinName) {
			if (!static::$recordValues['manyToManyJoinParams'][$joinName]) {
				$joinParams = static::$manyToManyJoins[$joinName];
				if (empty($joinParams)) { throw new Exception('Bah! join not defined!'); }
				$toClass = $joinParams['joinToClass'];
				if (empty($joinParams['joinTable'])) {
					$joinParams['joinTable'] = static::$tableName . '_' . $toClass::$tableName;
				}
				if (empty($joinParams['fromCol'])) {
					$joinParams['fromCol'] = substr(static::$tableName,0,-1) . '_id';
				}
				if (empty($joinParams['toCol'])) {
					$joinParams['toCol'] = substr($toClass::$tableName,0,-1) . '_id';
				}
				static::$recordValues['manyToManyJoinParams'][$joinName] = $joinParams;
			}
			return static::$recordValues['manyToManyJoinParams'][$joinName];
		}

		public function PersistManyToMany($ids, $joinName) {
			$joinParams = static::GetManyToManyJoinsParams($joinName);

			if (is_string($ids)) {
				$ids = explode(',', $ids);
			}

			$currentIds = $this->GetManyToManyIds($joinParams);

			foreach ($ids as $id) {
				if (empty($id)) continue;
				if (empty($currentIds[$id])) {
					$this->createManyToManyJoin($joinParams, $id);
				} else {
					unset($currentIds[$id]);
				}
			}

			foreach ($currentIds as $deletedId => $joinRecord) {
				$this->deleteManyToManyJoin($joinParams, $deletedId);
			}
		}

		private function GetManyToManyIds($joinParams) {
			$joins = ZDB::GetQuery('SELECT '.$joinParams['toCol'].' FROM '.$joinParams['joinTable'].' WHERE '.$joinParams['fromCol'].' = ?', [$this->id], ['keyedBy' => $joinParams['toCol']]);
			return $joins;
		}

		private function createManyToManyJoin($joinParams, $toId) {
			ZDB::Insert($joinParams['joinTable'], [$joinParams['fromCol'] => $this->id, $joinParams['toCol'] => $toId]);
		}

		private function deleteManyToManyJoin($joinParams, $toId) {
			ZDB::GetQuery('DELETE FROM '.$joinParams['joinTable'].' WHERE '.$joinParams['fromCol'].' = ? and '.$joinParams['toCol'].' = ?', [$this->id, $toId]);
		}

		public static function GetLabel($id) {
			$record = static::GetCached($id);
			if ($record) {
				return $record->GetDisplayName();
			}
			return '';
		}

		public static function GetCached($id) {
			if (empty($id)) {
				return null;	
			}
			static::GetAllCached();
			if (empty(static::$recordValues['allById'][$id])) {
				throw new Exception(static::getRecordName() . '::GetLabel('.$id.') failed.  Record does not exist', 1);	
			}
			return static::$recordValues['allById'][$id];
		}

		public static function GetAllCached() {
			if (empty(static::$recordValues['allById'])) {
				static::$recordValues['allById'] = static::GetAll(['keyedBy' => 'id']);
			}
			return static::$recordValues['allById'];
		}

		/**
		 * Finds the record where the criteria fields match.
		 * If the record exists, update the extraFields and save()
		 * If the record does not exist, it creates it with a merge of the criteria fields and the extrafields, and saves()
		 * 
		 */		
		public static function GetOrCreate($criteriaFields, $extraFields = []) {
			$record = static::GetSingleRecord($criteriaFields);
			if (!empty($record)) {
				$recordChanged = false;
				foreach ($extraFields as $fieldKey => $fieldValue) {
					if ($fieldValue != $record->data[$fieldKey]) {
						$record->data[$fieldKey] = $fieldValue;
						$recordChanged = true;
					}
				}
				if ($recordChanged) {
					$record->save();
				}
				return $record;
			}
			$record = new static(array_replace($criteriaFields, $extraFields));
			$record->save();
			return $record;
		}

		public static function GetSingleRecord($criteriaFields) {
			$wheres = [];
			foreach ($criteriaFields as $fieldKey => $fieldValue) {
				$wheres[] = ['column' => $fieldKey, 'value' => $fieldValue];
			}
			$records = static::GetAll(['where' => $wheres]);
			if (!empty($records)) {
				return $records[0];
			}
			return false;
		}

		public function getEditPageTitle() {
			if ($this->fromDB) {
				return 'Edit '.$this->getRecordName().': "' .$this->getDisplayName(). '"';
			}
			return 'Create new '.$this->getRecordName() . '...';
		}

	}

	