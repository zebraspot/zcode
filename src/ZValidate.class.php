<?php
	class ZValidate extends ZBase {
		public static function ValidateArray($arr, $ruleSet) {
			$errors = array();
			foreach($ruleSet as $key => $rule) {
				$err = self::ValidateValue($arr[$key], $rule);
				if ($err !== true) {
					$errors[$key] = array('error' => $err, 'value' => $arr[$key], 'rule' => $rule);
				}
				unset($arr[$key]);
			}
			if (!empty($arr)) {
				foreach ($arr as $key => $val) {
					$errors[$key] = array('error' => 'A value was provided, but none was expected', 'value' => $arr[$key], 'rule' => 'unexpected');
				}
			}
			if (empty($errors)) {
				return true;
			}
			return $errors;
		}

		public static function ValidateValue($value, $rule) {
			if ($rule === 'nonEmpty') {
				return self::ValidateNonEmpty($value);
			}
			if ($rule === 'integer') {
				return self::ValidateInteger($value);
			} 
			if ($rule === 'email') {
				return self::ValidateEmail($value);
			}
			return true;
		}
		
		public static function ValidateNonEmpty($value) {
			if (empty($value)) return 'The value was empty';
			return true;
		}

		public static function ValidateInteger($value) {
			if (!ctype_digit(strval($value))) return 'The value was not a valid integer';
			return true;
		}

		public static function ValidateEmail($value) {
			if (!filter_var($value, FILTER_VALIDATE_EMAIL)) return 'The value was not a valid email';
			return true;
		}

		public static function ErrorMessage($errors, $ruleMessages = false, $keyNames = false) {
			$out = 'The following errors were detected:<ul>';
			foreach ($errors as $key => $errorData) {
				$key = '<b>'.$key.'</b>';
				$message = $key.': '.$errorData['error'];
				$out .= '<li>' . $message . '</li>';
			}
			$out .= '</ul>';
			return $out;
		}

	}