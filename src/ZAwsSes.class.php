<?php
	// -----------------------------------------------------------------------------------------------------------
	// ------------------------------   Amazon SES (Simple Email Service) code   ---------------------------------------
	// -----------------------------------------------------------------------------------------------------------
	// Setup:
	// 	 create /.aws/config and /.aws/credentials as per https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html
	//   Add to site.conf.php:
	//			 'aws' => array(
	//			 	'ses' => array(
	//			 		'profile' => 'diyode_mail',
	//			 		'region' => 'us-west-2'
	//			 	)
	//			 )
	// Usage:
	// 	 ZAwsSes::SendMail('Testing email', 'Hey, this worky work?', 'simon@zebraspot.com', 'info@diyode.com');

	class ZAwsSes extends ZBase {
		public static $profile;
		public static $version = 'latest';
		public static $region;
		private static $client;
		
		public static function Init() {
			self::Config('aws/ses');
		}

		private static function getClient() {
			if (!(static::$client)) {
				static::$client = new Aws\Ses\SesClient([
					'profile' => static::$profile,
					'version' => static::$version,
					'region'  => static::$region
				]);
			}
			return static::$client;
		}

		public static function SendMail($subject, $message, $toAddress, $fromAddress = '', $fromName = '') {
			if (empty($fromAddress)) {
				$fromAddress = ZMessage::$defaultFrom;
			}
			if (!is_array($toAddress)) {
				$toAddress = [$toAddress];
			}
			return static::send([
				'Destination' => [
					'ToAddresses' => $toAddress,
				],
				'Message' => [
					'Body' => [
						'Html' => [
							'Charset' => 'UTF-8',
							'Data' => '<p>'.$message.'</p>',
						],
						'Text' => [
							'Charset' => 'UTF-8',
							'Data' => strip_tags($message),
						],
					],
					'Subject' => [
						'Charset' => 'UTF-8',
						'Data' => $subject,
					],
				],
				'ReplyToAddresses' => [$fromAddress],
				'Source' => $fromAddress,
			]);			
		}

		public static function send($mailData) {
			// $defaults = [  // eventually merge to avoid requiring defaults.
			// 	'Destination' => [ 'ToAddresses' => [] ],
			// 	'Message' => [ 
			// 		'Body' => [
			// 			'Html' => [ 'Charset' => 'UTF-8', 'Data' => '<p></p>' ],
			// 			'Text' => [ 'Charset' => 'UTF-8', 'Data' => '' ],
			// 		],
			// 		'Subject' => ['Charset' => 'UTF-8', 'Data' => ''],
			// 	],
			// 	'ReplyToAddresses' => [$defaultFrom],
			// 	'Source' => $defaultFrom,
			// ];

			$client = static::GetClient();
			return $client->sendEmail($mailData);			
		}

	}